/*
  Q Light Controller Plus
  onebyone.js

  Copyright (c) Jano Svitok

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

// List of colors to cycle through Hex format (RRGGBB)
// https://www.rapidtables.com/web/color/RGB_Color.html
const colors = [
    "ff6666", // Salmon
    "ffb266", // Peach
    "ffff66", // Sun
    "b2ff66", // Lime
    "66ff66", // Green
    "66ffb2", // Turqoise
    "66ffff", // Sky
    "66b2ff", // Baby Blue
    "6666ff", // Dusk
    "b266ff", // Lavendar
    "ff66ff", // Pink
    "ff66b2", // Lipstick
    "FF6347", // Tomato
    "F08080", // Light Coral
    "7CFC00", // Lawn Green
    "90EE90", // Light Green
    "00FA9A", // Medium Spring Green
    "00FFFF", // Cyan
    "7FFFD4", // Aqua Marine
    "6495ED", // Corn Flower Blue
    "00BFFF", // Deep Sky Blue
    "87CEFA", // Light Sky Blue
    "4169E1", // Royal Blue
    "8A2BE2", // Blue Violet
    "6A5ACD", // Slate Blue
    "9370DB", // Medium Purple
    "DDA0DD", // Plum
    "DA70D6", // Orchid
    "FFB6C1", // Light Pink
    "FFFACD", // Lemon Chiffon
    "FFFFE0", // Light Yellow
    "B0C4DE", // Light Steel Blue
    "E6E6FA", // Lavender
    "FFFAF0", // Floral White
    "F0F8FF", // Alice Blue
    "F8F8FF", // Ghost White
    "F0FFF0", // Honeydew
    "FFFFF0", // Ivory
    "F0FFFF", // Azure
];

var currentColor = "";

function getRandomColor() {
    newColor = colors[~~(Math.random() * colors.length)];
    // console.log("Random Color: " + newColor);
    return newColor
};

function getColor(step) {
    if (step !== 0) {
        return currentColor;
    }

    var oldColor = currentColor;
    while (oldColor === currentColor) {
        currentColor = getRandomColor();
    }

    return currentColor;
};

function getNewColor() {
    var oldColor = currentColor;
    while (oldColor === currentColor) {
        currentColor = getRandomColor();
    }

    return currentColor;
}

// Development tool access
var testAlgo;

(
    function () {
        var algo = new Object;
        algo.apiVersion = 2;
        algo.name = "One By One (Multicolor)";
        algo.author = "Jano Svitok & Frozen Tundra Entertainment";

        algo.acceptColors = 0; // Number of user selectable colors. (0-2)
        algo.properties = new Array();

        algo.rgbMap = function (width, height, rgb, step) {
            // Override the given RGB Color
            rgb = parseInt(getNewColor(), 16);

            var map = new Array(height);
            for (var y = 0; y < height; y++) {
                map[y] = new Array();
                for (var x = 0; x < width; x++) {
                    map[y][x] = 0;
                }
            }

            var xx = step % width;
            var yy = (step - xx) / width;
            map[yy][xx] = rgb;

            return map;
        };

        algo.rgbMapStepCount = function (width, height) {
            return width * height;
        };

        // Development tool access
        testAlgo = algo;

        return algo;
    }
)();
