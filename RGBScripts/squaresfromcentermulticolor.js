/*
  Q Light Controller Plus
  squaresfromcenter.js

  Copyright (c) David Garyga

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

// List of colors to cycle through Hex format (RRGGBB)
// https://www.rapidtables.com/web/color/RGB_Color.html
const colors = [
    "ff6666", // Salmon
    "ffb266", // Peach
    "ffff66", // Sun
    "b2ff66", // Lime
    "66ff66", // Green
    "66ffb2", // Turqoise
    "66ffff", // Sky
    "66b2ff", // Baby Blue
    "6666ff", // Dusk
    "b266ff", // Lavendar
    "ff66ff", // Pink
    "ff66b2", // Lipstick
    "FF6347", // Tomato
    "F08080", // Light Coral
    "7CFC00", // Lawn Green
    "90EE90", // Light Green
    "00FA9A", // Medium Spring Green
    "00FFFF", // Cyan
    "7FFFD4", // Aqua Marine
    "6495ED", // Corn Flower Blue
    "00BFFF", // Deep Sky Blue
    "87CEFA", // Light Sky Blue
    "4169E1", // Royal Blue
    "8A2BE2", // Blue Violet
    "6A5ACD", // Slate Blue
    "9370DB", // Medium Purple
    "DDA0DD", // Plum
    "DA70D6", // Orchid
    "FFB6C1", // Light Pink
    "FFFACD", // Lemon Chiffon
    "FFFFE0", // Light Yellow
    "B0C4DE", // Light Steel Blue
    "E6E6FA", // Lavender
    "FFFAF0", // Floral White
    "F0F8FF", // Alice Blue
    "F8F8FF", // Ghost White
    "F0FFF0", // Honeydew
    "FFFFF0", // Ivory
    "F0FFFF", // Azure
];

var currentColor = "";

function getRandomColor() {
    newColor = colors[~~(Math.random() * colors.length)];
    // console.log("Random Color: " + newColor);
    return newColor
};

function getColor(step) {
    if (step !== 0) {
        return;
    }

    var oldColor = currentColor;
    while (oldColor === currentColor) {
        currentColor = getRandomColor();
    }

    return;
};

// Development tool access
var testAlgo;

// Anonymous function that is called by QLC+
(
    function () {
        var algo = new Object;
        algo.apiVersion = 2;
        algo.name = "Squares From Center (MultiColor)";
        algo.author = "David Garyga & Frozen Tundra Entertainment";
        algo.acceptColors = 0; // Number of user selectable colors. (0-2)
        algo.properties = new Array();
        algo.fillSquares = 0;
        algo.properties.push("name:fillSquares|type:list|display:Fill squares|values:No,Yes|write:setFill|read:getFill");

        algo.setFill = function (_fill) {
            if (_fill === "Yes") { algo.fillSquares = 1; }
            else { algo.fillSquares = 0; }
        };

        algo.getFill = function () {
            if (algo.fillSquares === 1) { return "Yes"; }
            else { return "No"; }
        };

        algo.rgbMap = function (width, height, rgb, step) {
            var widthCenter = Math.floor((parseInt(width) + 1) / 2) - 1;
            var heightCenter = Math.floor((parseInt(height) + 1) / 2) - 1;
            var isWidthEven = (width % 2 === 0);
            var isHeightEven = (height % 2 === 0);

            // console.log("Requested Color: " + rgb);
            // console.log("Requested HEX: " + Number(rgb).toString(16));
            getColor(step);
            // console.log("Step: " + step + " Color: " + currentColor, "color:" + currentColor + " && " + parseInt(currentColor, 16));

            var map = new Array(height);
            for (var y = 0; y < height; y++) {
                map[y] = new Array();
                for (var x = 0; x < width; x++) {
                    if (algo.fillSquares === 1) {
                        if ((x <= widthCenter + step + (isWidthEven ? 1 : 0) && x >= widthCenter - step) &&
                            (y <= heightCenter + step + (isHeightEven ? 1 : 0) && y >= heightCenter - step)) {
                            map[y][x] = parseInt(currentColor, 16);
                        } else {
                            map[y][x] = 0;
                        }
                    }
                    else {
                        if ((x === widthCenter + step + (isWidthEven ? 1 : 0) || x === widthCenter - step) &&
                            (y <= heightCenter + step + (isHeightEven ? 1 : 0) && y >= heightCenter - step)) {
                            // map[y][x] = rgb;
                            map[y][x] = parseInt(currentColor, 16);
                        } else if ((y === heightCenter + step + (isHeightEven ? 1 : 0) || y === heightCenter - step) &&
                            (x <= widthCenter + step + (isWidthEven ? 1 : 0) && x >= widthCenter - step)) {
                            // map[y][x] = rgb;
                            map[y][x] = parseInt(currentColor, 16);
                        } else {
                            map[y][x] = 0;
                        }
                    }
                }
            }

            return map;
        };

        algo.rgbMapStepCount = function (width, height) {
            width = parseInt(width);
            height = parseInt(height);
            var max = width > height ? width : height;
            return Math.floor((max + 1) / 2);
        };

        // Development tool access
        testAlgo = algo;

        return algo;
    }
)();
