/*
  Q Light Controller Plus
  fill.js

  Copyright (c) Massimo Callegari

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0.txt

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

// List of colors to cycle through Hex format (RRGGBB)
// https://www.rapidtables.com/web/color/RGB_Color.html
const colors = [
    "ff6666", // Salmon
    "ffb266", // Peach
    "ffff66", // Sun
    "b2ff66", // Lime
    "66ff66", // Green
    "66ffb2", // Turqoise
    "66ffff", // Sky
    "66b2ff", // Baby Blue
    "6666ff", // Dusk
    "b266ff", // Lavendar
    "ff66ff", // Pink
    "ff66b2", // Lipstick
    "FF6347", // Tomato
    "F08080", // Light Coral
    "7CFC00", // Lawn Green
    "90EE90", // Light Green
    "00FA9A", // Medium Spring Green
    "00FFFF", // Cyan
    "7FFFD4", // Aqua Marine
    "6495ED", // Corn Flower Blue
    "00BFFF", // Deep Sky Blue
    "87CEFA", // Light Sky Blue
    "4169E1", // Royal Blue
    "8A2BE2", // Blue Violet
    "6A5ACD", // Slate Blue
    "9370DB", // Medium Purple
    "DDA0DD", // Plum
    "DA70D6", // Orchid
    "FFB6C1", // Light Pink
    "FFFACD", // Lemon Chiffon
    "FFFFE0", // Light Yellow
    "B0C4DE", // Light Steel Blue
    "E6E6FA", // Lavender
    "FFFAF0", // Floral White
    "F0F8FF", // Alice Blue
    "F8F8FF", // Ghost White
    "F0FFF0", // Honeydew
    "FFFFF0", // Ivory
    "F0FFFF", // Azure
];

const ORIENTATION_HORIZONTAL          = 0
const ORIENTATION_VERTICAL            = 1
const ORIENTATION_HORIZONTAL_REVERSED = 2
const ORIENTATION_VERTICAL_REVERSED   = 3

// Development tool access
var testAlgo;

function optionProperties(properties) {
    // Convert the properties object to an array of key-value pairs
    // const propertyArray = Object.entries(properties);
    const propertyArray = Object.keys(properties).map(function(key) {
        return [key, properties[key]];
    });
    

    // Map each key-value pair to a string in the format "key:value"
    // const propertyStrings = propertyArray.map(([key, value]) => `${key}:${value}`);
    const propertyStrings = propertyArray.map(function(pair) {
        return pair.join(':');
    });
      

    // Join the array of strings using the pipe character as a separator
    return propertyStrings.join('|');
}


(
    function () {
        var algo = new Object;
        algo.apiVersion = 2;
        algo.name = "Fill (Multicolor)";
        algo.author = "Massimo Callegari & Frozen Tundra Entertainment";
        algo.enableConsole = false;

        algo.acceptColors = 0; // Number of user selectable colors. (0-2)
        algo.orientation = ORIENTATION_HORIZONTAL;
        algo.properties = new Array();
        // algo.properties.push("name:orientation|type:list|display:Orientation|values:Horizontal,Vertical,Horizontal Reversed,Vertical Reversed|write:setOrientation|read:getOrientation");
        algo.properties.push(optionProperties({
            name: "orientation",
            type: "list",
            display: "Orientation",
            values: "Horizontal,Vertical,Horizontal Reversed,Vertical Reversed",
            write: "setOrientation",
            read: "getOrientation",
        }));

        algo.setOrientation = function (_orientation) {
            if (_orientation === "Vertical Reversed") { algo.orientation = ORIENTATION_VERTICAL_REVERSED; }
            else if (_orientation === "Horizontal Reversed") { algo.orientation = ORIENTATION_HORIZONTAL_REVERSED; }
            else if (_orientation === "Vertical") { algo.orientation = ORIENTATION_VERTICAL; }
            else { algo.orientation = ORIENTATION_HORIZONTAL; }
        };

        algo.getOrientation = function () {
            if (algo.orientation === ORIENTATION_VERTICAL_REVERSED) { return "Vertical Reversed"; }
            else if (algo.orientation === ORIENTATION_HORIZONTAL_REVERSED) { return "Horizontal Reversed"; }
            else if (algo.orientation === ORIENTATION_VERTICAL) { return "Vertical"; }
            else { return "Horizontal"; }
        };

        algo.algorithm = algo.getColorFromList;
        algo.algorithmName = "Provided Colors"

        algo.properties.push(optionProperties({
            name: "algorithm",
            type: "list",
            display: "Random Algorithm",
            values: "Provided Colors, True Random",
            write: "setAlgorithm",
            read: "getAlgorithm",
        }));
        
        algo.setAlgorithm = function (_algorithm) {
            if (_algorithm === "Provided Colors") {
                algo.algorithm = algo.getColorFromList;
                algo.algorithmName = "Provided Colors";
            } else {
                algo.algorithm = algo.getRandomColor;
                algo.algorithmName = "True Random";
            }
        };
        
        algo.getAlgorithm = function () {
            return algo.algorithmName;
        };

        algo.colorList = [];
        
        algo.properties.push(optionProperties({
            name: "colorList",
            type: "string",
            display: "Color List",
            write: "setColorList",
            read: "getColorList",
        }));

        algo.setColorList = function (_colors) {
            // algo.colorList = _colors.split(/\s*,\s*/).map(color => color.trim());
            algo.colorList = _colors.split(/\s*,\s*/).map(function(color) {
                return color.trim();
            });
        };

        algo.getColorList = function () {
            if (algo.colorList.length === 0) {
                algo.colorList = colors;
            }

            return algo.colorList.toString()
        };

        algo.generateNewColorOnEveryStep = false;
        
        algo.properties.push(optionProperties({
            name: "generateNewColorOnEveryStep",
            type: "list",
            display: "Generate Every Step",
            values: "Yes,No",
            write: "setGenerate",
            read: "getGenerate",
        }));
        
        algo.setGenerate = function (_generate) {
            algo.generateNewColorOnEveryStep = _generate === "Yes" ? true : false;
        };
        
        algo.getGenerate = function () {
            return algo.generateNewColorOnEveryStep ? "Yes" : "No";
        };

        algo.getColorFromList = function () {
            var oldColor = algo.currentColor;
            while (oldColor === algo.currentColor) {
                var index = ~~(Math.random() * algo.colorList.length)
                algo.currentColor = algo.colorList[index];
            }
        }

        algo.getRandomColor = function () {
            // Generate random values for R, G, and B
            const r = Math.floor(Math.random() * 256);
            const g = Math.floor(Math.random() * (256 - r));
            const b = 255 - r - g;
        
            // Convert values to HEX format
            const hexR = r.toString(16).padStart(2, '0');
            const hexG = g.toString(16).padStart(2, '0');
            const hexB = b.toString(16).padStart(2, '0');
        
            // Concatenate and return the HEX value
            // algo.currentColor = `${hexR}${hexG}${hexB}`;
            algo.currentColor = hexR + hexG + hexB;
        }

        algo.currentColor = colors[0]
        algo.prevStep = 0

        algo.getColor = function (step) {
            if (algo.generateNewColorOnEveryStep || Math.abs(step - algo.prevStep) > 1) {
                algo.algorithm();
                if (algo.enableConsole) {
                    console.log("Random Color [" + step + "]: " + algo.currentColor);
                }
            }
        
            algo.prevStep = step;
            return algo.currentColor;
        };
        

        algo.rgbMap = function (width, height, rgb, step) {
            // Override the given RGB Color
            rgb = parseInt(algo.getColor(step), 16);

            var map = new Array(height);
            for (var y = 0; y < height; y++) {
                map[y] = new Array();
                for (var x = 0; x < width; x++) {
                    if ((algo.orientation === ORIENTATION_HORIZONTAL && x <= step) ||
                        (algo.orientation === ORIENTATION_VERTICAL && y <= step) ||
                        (algo.orientation === ORIENTATION_HORIZONTAL_REVERSED && x >= width - step - 1) ||
                        (algo.orientation === ORIENTATION_VERTICAL_REVERSED && y >= height - step - 1)) {
                        map[y][x] = rgb;
                    } else {
                        map[y][x] = 0;
                    }
                }
            }

            return map;
        };

        algo.rgbMapStepCount = function (width, height) {
            if (algo.orientation === ORIENTATION_HORIZONTAL || algo.orientation === ORIENTATION_HORIZONTAL_REVERSED) {
                return width;
            } else {
                return height;
            }
        };

        // Development tool access
        testAlgo = algo;
        // testAlgo.enableConsole = true;

        return algo;
    }
)();
