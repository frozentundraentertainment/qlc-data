(
  function() 
  {
  var algo = {};
  algo.apiVersion = 2;
  algo.name = "Game of Life";
  algo.author = "ChatGPT & wolskey";
  algo.properties = [];

  var grid = [];

  algo.setGridSize = function(w, h) {
    width = w;
    height = h;
    grid = [];
    for (var y = 0; y < height; y++) {
      var row = [];
      for (var x = 0; x < width; x++) {
        row.push(Math.random() > 0.5 ? 1 : 0);
      }
      grid.push(row);
    }
  };

  algo.getGridSize = function() {
    return [width, height];
  };

  algo.rgbMap = function(w, h, rgb, step) {
    if (w != width || h != height) {
      algo.setGridSize(w, h);
    }

    var map = [];
    for (var y = 0; y < height; y++) {
      var row = [];
      for (var x = 0; x < width; x++) {
        var neighbors = 0;
        for (var i = -1; i <= 1; i++) {
          for (var j = -1; j <= 1; j++) {
            if (i == 0 && j == 0) {
              continue;
            }
            var _y = (y + i + height) % height;
            var _x = (x + j + width) % width;
            if (grid[_y][_x]) {
              neighbors++;
            }
          }
        }

        var state = grid[y][x];
        if (state && (neighbors < 2 || neighbors > 3)) {
          state = 0;
        } else if (!state && neighbors == 3) {
          state = 1;
        }

        grid[y][x] = state;
        row.push(state ? rgb : 0);
      }
      map.push(row);
    }

    return map;
  };

  algo.rgbMapStepCount = function(w, h) {
    return 2 * Math.max(w, h);
  };

  return algo;
})();