param (
    [Parameter(Mandatory=$true)]
    [string]$CSVFilePath = (Join-Path -Path $PSScriptRoot -ChildPath "RGBAW_Complete.csv"),

    [Parameter(Mandatory=$true)]
    [int]$FixtureGroup,

    [Parameter(Mandatory=$true)]
    [string]$FixturePrefix,

    [Parameter(Mandatory=$true)]
    [string]$FunctionPathPrefix,

    [int]$NumberOfZones = 1,

    [int]$NumberOfRows = 1,
    
    # Check if multiple rows snake back and forth or scan like a CRT monitor.
    [switch]$RowsSnake = $false,

    [Parameter(Mandatory=$true)]
    [array]$RedChannelGroupIDs,

    [Parameter(Mandatory=$true)]
    [array]$GreenChannelGroupIDs,

    [Parameter(Mandatory=$true)]
    [array]$BlueChannelGroupIDs,

    [array]$AmberChannelGroupIDs,

    [array]$WhiteChannelGroupIDs,

    [array]$UVChannelGroupIDs,

    [int]$StrobeChannelGroupID = -1,

    [Parameter(Mandatory=$true)]
    [array]$FixtureIDs,

    [Parameter(Mandatory=$true)]
    [array]$RedFixtureChannels,

    [Parameter(Mandatory=$true)]
    [array]$GreenFixtureChannels,

    [Parameter(Mandatory=$true)]
    [array]$BlueFixtureChannels,

    [array]$AmberFixtureChannels,

    [array]$WhiteFixtureChannels,

    [array]$UVFixtureChannels,
    
    [int]$StrobeFixtureChannel = -1,

    [int]$StrobeValueSlow = 15,

    [int]$StrobeValueMedium = 17,

    [int]$StrobeValueFast = 20,

    [int]$StrobeMaxHz,

    [switch]$DisableAmber = $false,

    [switch]$DisableWhite = $false
)

$ErrorActionPreference = "Stop"
$StrobeEnable = $false

# Pre-Populate an array for White/Amber values when those channels are disabled.
# This will allow easier logic in later functions.
$defaultChannelIDs = @(0) * $NumberOfZones

#region Channel Group Count Check
if ($RedChannelGroupIDs.Count -ne $NumberOfZones) {
    Write-Error "Error: The number of RED Channel Groups is [$($RedChannelGroupIDs.Count)] expects: $NumberOfZones."
    exit 1
}

if ($GreenChannelGroupIDs.Count -ne $NumberOfZones) {
    Write-Error "Error: The number of GREEN Channel Groups is [$($GreenChannelGroupIDs.Count)] expects: $NumberOfZones."
    exit 1
}

if ($BlueChannelGroupIDs.Count -ne $NumberOfZones) {
    Write-Error "Error: The number of BLUE Channel Groups is [$($BlueChannelGroupIDs.Count)] expects: $NumberOfZones."
    exit 1
}

if ($AmberChannelGroupIDs.Count -ne $NumberOfZones) {
    if ($AmberChannelGroupIDs.Count -gt 0) {
        Write-Error "Error: The number of AMBER Channel Groups is [$($AmberChannelGroupIDs.Count)] expects: $NumberOfZones."
        exit 1
    } else {
        $DisableAmber = $true
        $AmberChannelGroupIDs = $defaultChannelIDs
    }
}

if ($WhiteChannelGroupIDs.Count -ne $NumberOfZones) {
    if ($AWhiteChannelGroupIDs.Count -gt 0) {
        Write-Error "Error: The number of WHITE Channel Groups is [$($WhiteChannelGroupIDs.Count)] expects: $NumberOfZones."
        exit 1
    } else {
        $DisableWhite = $true
        $WhiteChannelGroupIDs = $defaultChannelIDs
    }
}
#endregion

#region Fixture Channel Count Check
if ($RedFixtureChannels.Count -ne $NumberOfZones) {
    Write-Error "Error: The number of RED Fixture Channels is [$($RedFixtureChannels.Count)] expects: $NumberOfZones."
    exit 1
}

if ($GreenFixtureChannels.Count -ne $NumberOfZones) {
    Write-Error "Error: The number of GREEN Fixture Channels is [$($GreenFixtureChannels.Count)] expects: $NumberOfZones."
    exit 1
}

if ($BlueFixtureChannels.Count -ne $NumberOfZones) {
    Write-Error "Error: The number of BLUE Fixture Channels is [$($BlueFixtureChannels.Count)] expects: $NumberOfZones."
    exit 1
}

if ($AmberFixtureChannels.Count -ne $NumberOfZones) {
    if ($AmberFixtureChannels.Count -gt 0) {
        Write-Error "Error: The number of AMBER Fixture Channels is [$($AmberFixtureChannels.Count)] expects: $NumberOfZones."
        exit 1
    } else {
        $DisableAmber = $true
        $AmberFixtureChannels = $defaultChannelIDs
    }
}

if ($WhiteFixtureChannels.Count -ne $NumberOfZones) {
    if ($WhiteFixtureChannels.Count -gt 0) {
        Write-Error "Error: The number of WHITE Fixture Channels is [$($WhiteFixtureChannels.Count)] expects: $NumberOfZones."
        exit 1
    } else {
        $DisableWhite = $true
        $WhiteFixtureChannels = $defaultChannelIDs
    }
}
#endregion

#region Assign RGBAW Channels
class RGBAW {
    [int]$Red
    [int]$Green
    [int]$Blue
    [int]$Amber
    [int]$White

    static [RGBAW] Default() {
        return [RGBAW]::new(0, 0, 0, 0, 0)
    }

    RGBAW([int]$Red, [int]$Green, [int]$Blue, [int]$Amber, [int]$White) {
        $this.Red = $Red
        $this.Green = $Green
        $this.Blue = $Blue
        $this.Amber = $Amber
        $this.White = $White
    }

    # Override ToString method
    [string] ToString() {
        return "Red: {0:D3}, Green: {1:D3}, Blue: {2:D3}, Amber: {3:D3}, White: {4:D3}" -f $this.Red, $this.Green, $this.Blue, $this.Amber, $this.White
    }
}

$ChannelGroups = [System.Collections.ArrayList]@()
$FixtureChannels = [System.Collections.ArrayList]@()

for ($currentZone = 0; $currentZone -lt $NumberOfZones; $currentZone++) {
    try {
        $channelGroup = [RGBAW]::new(
            $RedChannelGroupIDs[$currentZone],
            $GreenChannelGroupIDs[$currentZone],
            $BlueChannelGroupIDs[$currentZone],
            $AmberChannelGroupIDs[$currentZone],
            $WhiteChannelGroupIDs[$currentZone]
        )
        # Write-Host "Channel Group:    $channelGroup"

        $fixtureChannel = [RGBAW]::new(
            $RedFixtureChannels[$currentZone],
            $GreenFixtureChannels[$currentZone],
            $BlueFixtureChannels[$currentZone],
            $AmberFixtureChannels[$currentZone],
            $WhiteFixtureChannels[$currentZone]
        )
        # Write-Host "Fixture Channels: $fixtureChannel"
    }
    catch [System.IndexOutOfRangeException] {
        Write-Error "Channel Groups or Fixture Channels do not match number of zones!"
        exit 1
    }

    [void]$ChannelGroups.Add($channelGroup)
    [void]$FixtureChannels.Add($fixtureChannel)
}
#endregion

#region Misc Checks
# Ensure required Strobe parameters are set.
if ($StrobeChannelGroupID -lt 0 -and $StrobeFixtureChannel -lt 0) {
    $StrobeChannelGroupID = $null
    $StrobeFixtureChannel = $null
    $StrobeMaxHz = $null
    $StrobeValueSlow = $null
    $StrobeValueMedium = $null
    $StrobeValueFast = $null
} else {
    if ($StrobeChannelGroupID -lt 0) {
        Write-Error "ERROR: -StrobeChannelGroupID must be set if -StrobeFixtureChannel is set."
        exit 1
    }
    if ($StrobeFixtureChannel -lt 0) {
        Write-Error "ERROR: -StrobeFixtureChannel must be set if -StrobeChannelGroupID is set."
        exit 1
    }

    $StrobeEnable = $true
}

# Check that the number of zones can be distributed evenly amongst all rows.
if ($NumberOfZones % $NumberOfRows -ne 0) {
    Write-Host "Zones [$NumberOfZones] cannot be evenly distributed amongst [$NumberOfRows] rows."
}

# Check if the CSV file exists
if (Test-Path -Path $CSVFilePath -PathType Leaf) {
    # The file exists, proceed with reading
    $colors = Import-Csv -Path $CSVFilePath
    Write-Host "CSV file loaded successfully. Found [$($colors.Count)] colors!"
} else {
    # The file does not exist
    Write-Host "Error: CSV file not found at $CSVFilePath"
    exit 1
}
#endregion

$FunctionMultiplier = 100000
$FunctionID = ($FixtureGroup * $FunctionMultiplier)
$functions = @()

#region Special Functions
function Get-DMXFromHz {
    param (

    [Parameter(Mandatory=$true)]
        [int]$Frequency,

        [Parameter(Mandatory=$true)]
        [int]$MaxFrequency
    )

    return [math]::floor(($Frequency * 255) / $MaxFrequency)
}

function Get-ChannelData {
    param (
        [Parameter(Mandatory = $true)]
        [RGBAW]$ChannelGroupIDs,

        [Parameter(Mandatory = $true)]
        [RGBAW]$FixtureChannelIDs,

        [Parameter(Mandatory = $true)]
        [RGBAW]$ColorValues,

        [switch]$DisableAmber = $false,
    
        [switch]$DisableWhite = $false
    )

    $channelGroupData = [System.Collections.ArrayList]@()
    $fixtureChannelData = [System.Collections.ArrayList]@()
    
            
    [void]$channelGroupData.Add($ChannelGroupIDs.Red)
    [void]$channelGroupData.Add($ColorValues.Red)
    [void]$channelGroupData.Add($ChannelGroupIDs.Green)
    [void]$channelGroupData.Add($ColorValues.Green)
    [void]$channelGroupData.Add($ChannelGroupIDs.Blue)
    [void]$channelGroupData.Add($ColorValues.Blue)

    [void]$fixtureChannelData.Add($FixtureChannelIDs.Red)
    [void]$fixtureChannelData.Add($ColorValues.Red)
    [void]$fixtureChannelData.Add($FixtureChannelIDs.Green)
    [void]$fixtureChannelData.Add($ColorValues.Green)
    [void]$fixtureChannelData.Add($FixtureChannelIDs.Blue)
    [void]$fixtureChannelData.Add($ColorValues.Blue)

    if (-not $DisableAmber) {
        [void]$channelGroupData.Add($ChannelGroupIDs.Amber)
        [void]$channelGroupData.Add($ColorValues.Amber)
        [void]$fixtureChannelData.Add($FixtureChannelIDs.Amber)
        [void]$fixtureChannelData.Add($ColorValues.Amber)
    }
    if (-not $DisableWhite) {
        [void]$channelGroupData.Add($ChannelGroupIDs.White)
        [void]$channelGroupData.Add($ColorValues.White)
        [void]$fixtureChannelData.Add($FixtureChannelIDs.White)
        [void]$fixtureChannelData.Add($ColorValues.White)
    }

    # Return a custom object with named properties
    return [PSCustomObject]@{
        ChannelGroupData = $channelGroupData
        FixtureChannelData = $fixtureChannelData
    }
}

function Format-Xml {
    param (
        [string]$xmlString
    )

    $xmlDoc = New-Object System.Xml.XmlDocument
    $xmlDoc.LoadXml($xmlString)
    $stringWriter = New-Object System.IO.StringWriter
    $xmlWriter = New-Object System.Xml.XmlTextWriter $stringWriter
    $xmlWriter.Formatting = 'Indented'
    $xmlDoc.WriteContentTo($xmlWriter)
    $xmlWriter.Flush()
    $stringWriter.Flush()
    
    return $stringWriter.ToString()
}

# <Function ID="429" Type="Scene" Name="24 [Z3] Cyan" Path="COLORBand/24 Cyan">
# <Speed FadeIn="0" FadeOut="0" Duration="0"/>
# <ChannelGroupsVal>63,28,64,145,65,107,66,0,67,18</ChannelGroupsVal>
# <FixtureVal ID="32">12,28,13,145,14,107,15,0,16,18</FixtureVal>
# <FixtureVal ID="33">12,28,13,145,14,107,15,0,16,18</FixtureVal>
# </Function>
function Get-SceneObject {
    param (
        [Parameter(Mandatory = $true)]
        [string]$FunctionID,

        [Parameter(Mandatory = $true)]
        [string]$sceneName,

        [Parameter(Mandatory = $true)]
        [string]$FunctionPath,

        [Parameter(Mandatory = $true)]
        [array]$channelGroupsData,

        [Parameter(Mandatory = $true)]
        [array]$FixtureIDs,

        [Parameter(Mandatory = $true)]
        [array]$FixtureChannelPairs
    )

    $fixtureVals = @()

    foreach ($fixtureID in $FixtureIDs) {
        $fixtureObject = @{
            ID = "$fixtureID"
            Values = $FixtureChannelPairs -join ','
        }

        $fixtureVals += $fixtureObject
    }

    $functionXML = @"
<Function ID="$FunctionID" Type="Scene" Name="$sceneName" Path="$FunctionPath">
    <Speed FadeIn="0" FadeOut="0" Duration="0"/>
    <ChannelGroupsVal>$($channelGroupsData -join ',')</ChannelGroupsVal>
    $($FixtureVals | ForEach-Object {
        $fixtureID = $_.ID
        $fixtureValues = $_.Values
        "<FixtureVal ID=""$fixtureID"">$fixtureValues</FixtureVal>"
    })
    </Function>
"@

    return Format-Xml -xmlString $functionXML
}

class Step {
    [int]$FadeIn
    [int]$Hold
    [int]$FadeOut
    [int]$FunctionID

    Step([int]$FunctionID) {
        $this.FadeIn = 0
        $this.Hold = 500
        $this.FadeOut = 0
        $this.FunctionID = $FunctionID
    }

    [string] ToXML([int]$StepNumber) {
        return "<Step Number=`"{0:D2}`" FadeIn=`"$($this.FadeIn)`" Hold=`"$($this.Hold)`" FadeOut=`"$($this.FadeOut)`">$($this.FunctionID)</Step>" -f $StepNumber
    }
}

# <Function ID="0" Type="Chaser" Name="WFXH - 01 Magma [Positions]" Path="Wash FX HEX">
# <Speed FadeIn="0" FadeOut="0" Duration="0"/>
# <Direction>Forward</Direction>
# <RunOrder>Loop</RunOrder>
# <SpeedModes FadeIn="Default" FadeOut="Default" Duration="Common"/>
# <Step Number="0" FadeIn="0" Hold="0" FadeOut="0">52</Step>
# <Step Number="1" FadeIn="0" Hold="0" FadeOut="0">50</Step>
# <Step Number="2" FadeIn="0" Hold="0" FadeOut="0">47</Step>
# <Step Number="3" FadeIn="0" Hold="0" FadeOut="0">48</Step>
# <Step Number="4" FadeIn="0" Hold="0" FadeOut="0">46</Step>
# <Step Number="5" FadeIn="0" Hold="0" FadeOut="0">49</Step>
# <Step Number="6" FadeIn="0" Hold="0" FadeOut="0">51</Step>
# </Function>
function Get-ChaseObject {
    param (
        [Parameter(Mandatory = $true)]
        [string]$FunctionID,

        [Parameter(Mandatory = $true)]
        [string]$ChaseName,

        [Parameter(Mandatory = $true)]
        [string]$FunctionPath,

        [int]$CommonFadeIn = 0,

        [int]$CommonFadeOut = 0,

        [int]$CommonFadeDuration = 500,

        [ValidateSet("Forward", "Backward")]
        [string]$Direction = "Forward",

        [ValidateSet("Loop", "SingleShot", "PingPong", "Random")]
        [string]$RunOrder = "Loop",

        [ValidateSet("Default", "PerStep", "Common")]
        [string]$FadeInMode = "Default",

        [ValidateSet("Default", "PerStep", "Common")]
        [string]$FadeOutMode = "Default",

        [ValidateSet("PerStep", "Common")]
        [string]$DurationMode = "Common",
        
        [Parameter(Mandatory = $true)]
        [Step[]]$Steps
    )

    $index = 0
    $functionXML = @"
<Function ID="$FunctionID" Type="Chaser" Name="$ChaseName" Path="$FunctionPath">
    <Speed FadeIn="$CommonFadeIn" FadeOut="$CommonFadeOut" Duration="$CommonFadeDuration"/>
    <Direction>$Direction</Direction>
    <RunOrder>$RunOrder</RunOrder>
    <SpeedModes FadeIn="$FadeInMode" FadeOut="$FadeOutMode" Duration="$DurationMode"/>
    $($Steps | ForEach-Object {
        $_.ToXML($index)
        $index++
    })
    </Function>
"@

    return Format-Xml -xmlString $functionXML
}

function Get-ChaseStyles {
    param (
        [Parameter(Mandatory = $true)]
        [int]$FunctionID,

        [Parameter(Mandatory = $true)]
        [string]$NamePrefix,

        [Parameter(Mandatory = $true)]
        [string]$ChasePath,

        [Parameter(Mandatory = $true)]
        [Step[]]$Steps,

        [switch]$Forward,
        [switch]$Backward,
        [switch]$Loop,
        [switch]$SingleShot,
        [switch]$PingPong,
        [switch]$Random
    )

    $functions = [System.Collections.ArrayList]@()
    $directions = [System.Collections.ArrayList]@()
    $runOrders = [System.Collections.ArrayList]@()

    if ($Forward -or -not $Backward) {
        [void]$directions.Add("Forward")
    }
    if ($Backward -and ($Steps.Count -gt 2)) {
        [void]$directions.Add("Backward")
    }

    if ($Loop -or -not ($SingleShot -or $PingPong -or $Random)) {
        [void]$runOrders.Add("Loop")
    }
    if ($SingleShot) {
        [void]$runOrders.Add("SingleShot")
    }
    if ($PingPong -and ($Steps.Count -gt 2)) {
        [void]$runOrders.Add("PingPong")
    }
    if ($Random -and ($Steps.Count -gt 2)) {
        [void]$runOrders.Add("Random")
    }

    foreach ($direction in $directions) {
        foreach ($runOrder in $runOrders) {
            [void]$functions.Add((Get-ChaseObject `
                -FunctionID $FunctionID `
                -ChaseName "$NamePrefix [$direction-$runOrder]" `
                -FunctionPath $ChasePath `
                -Direction $direction `
                -RunOrder $runOrder `
                -Steps $Steps
            ))

            $FunctionID++
        }
    }

    return @{
        Functions = $functions
        NextID = $FunctionID
    }
}
#endregion

#region Scene Generation Functions
function Get-AllZones {
    param (
        [Parameter(Mandatory = $true)]
        [int]$NumberOfZones,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$ChannelGroups,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$FixtureChannels,

        [Parameter(Mandatory = $true)]
        [RGBAW]$ColorValues,

        [switch]$DisableAmber = $false,
    
        [switch]$DisableWhite = $false
    )
    
    $channelGroupData = [System.Collections.ArrayList]@()
    $fixtureChannelData = [System.Collections.ArrayList]@()

    for ($currentZone = 0; $currentZone -lt $NumberOfZones; $currentZone++) {

        $channelData = Get-ChannelData `
            -ChannelGroupIDs $ChannelGroups[$currentZone] `
            -FixtureChannelIDs $FixtureChannels[$currentZone] `
            -ColorValues $ColorValues `
            -DisableAmber:$DisableAmber `
            -DisableWhite:$DisableWhite

        [void]$channelGroupData.AddRange($channelData.ChannelGroupData)
        [void]$fixtureChannelData.AddRange($channelData.fixtureChannelData)
    }

    $zoneData = @{
        Name = "Full"
        ChannelGroupData = $channelGroupData
        FixtureChannelData = $fixtureChannelData
    }

    return New-Object PSObject -Property $zoneData
}

function Get-IndividualZones {
    param (
        [Parameter(Mandatory = $true)]
        [int]$NumberOfZones,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$ChannelGroups,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$FixtureChannels,

        [Parameter(Mandatory = $true)]
        [RGBAW]$ColorValues,

        [switch]$DisableAmber = $false,
    
        [switch]$DisableWhite = $false
    )

    $zones = @()

    for ($activeZone = 1; $activeZone -le $NumberOfZones; $activeZone++) {
        $channelGroupData = [System.Collections.ArrayList]@()
        $fixtureChannelData = [System.Collections.ArrayList]@()

        for ($currentZone = 1; $currentZone -le $NumberOfZones; $currentZone++) {
            $activeColorValues = if ($activeZone -eq $currentZone) { $ColorValues } else { [RGBAW]::Default() }

            $channelData = Get-ChannelData `
                -ChannelGroupIDs $ChannelGroups[$currentZone -1] `
                -FixtureChannelIDs $FixtureChannels[$currentZone -1] `
                -ColorValues $activeColorValues `
                -DisableAmber:$DisableAmber `
                -DisableWhite:$DisableWhite

            [void]$channelGroupData.AddRange($channelData.ChannelGroupData)
            [void]$fixtureChannelData.AddRange($channelData.fixtureChannelData)
        }

        $zoneData = @{
            Name = "Z$activeZone"
            ChannelGroupData = $channelGroupData
            FixtureChannelData = $fixtureChannelData
        }

        $zones += New-Object PSObject -Property $zoneData
    }

    return $zones
}

# Creates Z1, Z1+Z2, Z1+Z2+Z3, Z2+Z3, Z1+Z3, etc...
# Unfortunately this function creates an astronomical number of functions once you get above 3 zones.
# 2^ZONENUMBER * NUMBEROFCOLORS
# For example, if you have 3 zones and 40 lights 2^3*40 = 320 functions
# If you have 6 zones and 40 lights 2^6*40 = 2560 functions
function Get-ZoneCombinations {
    param (
        [Parameter(Mandatory = $true)]
        [int]$NumberOfZones,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$ChannelGroups,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$FixtureChannels,

        [Parameter(Mandatory = $true)]
        [RGBAW]$ColorValues,

        [switch]$DisableAmber = $false,
    
        [switch]$DisableWhite = $false
    )

    $zones = @()

    # This loop iterates over the possible values of $bitMask from 1 to 2^$NumberOfZones - 1.
    # This is a common pattern in bit manipulation where each bit in the binary representation of $bitMask corresponds to a specific element or condition.
    $bitMaskMax = [math]::pow(2, $NumberOfZones)
    for ($bitMask = 1; $bitMask -lt $bitMaskMax; $bitMask++) {

        $channelGroupData = [System.Collections.ArrayList]@()
        $fixtureChannelData = [System.Collections.ArrayList]@()
        $activeZones = [System.Collections.ArrayList]@()

        for ($zone = 0; $zone -lt $NumberOfZones; $zone++) {
            if (($bitMask -band [math]::pow(2, $zone)) -ne 0) {
                [void]$activeZones.Add($zone + 1)
                $activeColorValues = $ColorValues
            } else {
                $activeColorValues = [RGBAW]::Default()
            }

            $channelData = Get-ChannelData `
                -ChannelGroupIDs $ChannelGroups[$zone] `
                -FixtureChannelIDs $FixtureChannels[$zone] `
                -ColorValues $activeColorValues `
                -DisableAmber:$DisableAmber `
                -DisableWhite:$DisableWhite

            [void]$channelGroupData.AddRange($channelData.ChannelGroupData)
            [void]$fixtureChannelData.AddRange($channelData.fixtureChannelData)
        }

        $zoneData = @{
            Name = "Z" + ($activeZones -join '+Z')
            ChannelGroupData = $channelGroupData
            FixtureChannelData = $fixtureChannelData
        }

        $zones += New-Object PSObject -Property $zoneData
    }

    return $zones
}

function Get-ZoneWave {
    param (
        [Parameter(Mandatory = $true)]
        [int]$NumberOfZones,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$ChannelGroups,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$FixtureChannels,

        [Parameter(Mandatory = $true)]
        [RGBAW]$ColorValues,

        [switch]$DisableAmber = $false,
    
        [switch]$DisableWhite = $false
    )

    $allZones = @()

    # Loop through zones twice
    for ($iteration = 1; $iteration -le (2 * $NumberOfZones - 1); $iteration++) {
        $activeZones = @()

        $channelGroupData = [System.Collections.ArrayList]@()
        $fixtureChannelData = [System.Collections.ArrayList]@()
        $activeZones = [System.Collections.ArrayList]@()

        for ($zone = 1; $zone -le $NumberOfZones; $zone++) {
            if ($iteration -le $NumberOfZones) {
                # Activate each zone one by one
                if ($zone -le $iteration) {
                    [void]$activeZones.Add($zone)
                    $activeColorValues = $ColorValues
                } else {
                    $activeColorValues = [RGBAW]::Default()
                }
            } else {
                # Activate subsequent zones in reverse order
                if ($zone -ge ($iteration - $NumberOfZones + 1)) {
                    [void]$activeZones.Add($zone)
                    $activeColorValues = $ColorValues
                } else {
                    $activeColorValues = [RGBAW]::Default()
                }
            }

            $channelData = Get-ChannelData `
                -ChannelGroupIDs $ChannelGroups[$zone - 1] `
                -FixtureChannelIDs $FixtureChannels[$zone - 1] `
                -ColorValues $activeColorValues `
                -DisableAmber:$DisableAmber `
                -DisableWhite:$DisableWhite

            [void]$channelGroupData.AddRange($channelData.ChannelGroupData)
            [void]$fixtureChannelData.AddRange($channelData.fixtureChannelData)
        }

        $zoneData = @{
            Name = "Z" + ($activeZones -join '+Z')
            ChannelGroupData = $channelGroupData
            FixtureChannelData = $fixtureChannelData
        }
        $allZones += New-Object PSObject -Property $zoneData
    }

    return $allZones
}

function Get-AlternateZones {
    param (
        [Parameter(Mandatory = $true)]
        [int]$NumberOfZones,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$ChannelGroups,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$FixtureChannels,

        [Parameter(Mandatory = $true)]
        [RGBAW]$ColorValues,

        [switch]$Even = $false,

        [switch]$DisableAmber = $false,
    
        [switch]$DisableWhite = $false
    )
    
    $channelGroupData = [System.Collections.ArrayList]@()
    $fixtureChannelData = [System.Collections.ArrayList]@()

    if ($Even) {
        $currentZoneEvenColors = $ColorValues
        $currentZoneOddColors = [RGBAW]::Default()
        $functionName = "Even"
    } else {
        $currentZoneEvenColors = [RGBAW]::Default()
        $currentZoneOddColors = $ColorValues
        $functionName = "Odd"
    }

    for ($currentZone = 1; $currentZone -le $NumberOfZones; $currentZone++) {
        $activeColorValues = if ($currentZone % 2 -eq 0) { $currentZoneEvenColors } else { $currentZoneOddColors }

        $channelData = Get-ChannelData `
            -ChannelGroupIDs $ChannelGroups[$currentZone - 1] `
            -FixtureChannelIDs $FixtureChannels[$currentZone - 1] `
            -ColorValues $activeColorValues `
            -DisableAmber:$DisableAmber `
            -DisableWhite:$DisableWhite

        [void]$channelGroupData.AddRange($channelData.ChannelGroupData)
        [void]$fixtureChannelData.AddRange($channelData.fixtureChannelData)
    }

    return @{
        Name = "$functionName"
        ChannelGroupData = $channelGroupData
        FixtureChannelData = $fixtureChannelData
    }
}

function Get-VerticalZones {
    param (
        [Parameter(Mandatory = $true)]
        [int]$NumberOfZones,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$ChannelGroups,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$FixtureChannels,

        [Parameter(Mandatory = $true)]
        [RGBAW]$ColorValues,

        [switch]$DisableAmber = $false,
    
        [switch]$DisableWhite = $false,

        [Parameter(Mandatory = $true)]
        [int]$Rows,
        
        [switch]$Top,
        [switch]$Bottom,
        [switch]$Middle,
        [switch]$Outer
    )

    if (-not ($Top -or $Bottom -or $Middle)) {
        throw "One of -Top, -Bottom, -Middle, or -Outer must be selected!"
    }

    $zonesPerRow = [math]::Ceiling($NumberOfZones / $Rows)

    $functionName = if ($Top) {
        "Top"
    } elseif ($Bottom) {
        "Bottom"
    } elseif ($Middle) {
        "Middle"
    } elseif ($Outer) {
        "VOuter"
    }

    $channelGroupData = [System.Collections.ArrayList]@()
    $fixtureChannelData = [System.Collections.ArrayList]@()

    for ($row = 1; $row -le $Rows; $row++) {
        $activeRow = if ($Top) {
            ($row -eq 1)
        } elseif ($Bottom) {
            ($row -eq $Rows)
        } elseif ($Middle) {
            if ($Rows % 2 -eq 0) {
                # If $Rows is even, activate the two middle zones
                ($row -eq ($Rows / 2) -or $row -eq ($Rows / 2) + 1)
            } else {
                # If $Rows is odd, activate only the middle zone
                ($row -eq [math]::Ceiling($Rows / 2))
            }
        } elseif ($Outer) {
            # Top or Bottom row.
            ($row -in 1, $rows)
        }

        for ($col = 1; $col -le $zonesPerRow; $col++) {
            $zoneIndex = ($row - 1) * $zonesPerRow + $col - 1
            $activeColorValues = if ($activeRow) { $ColorValues } else { [RGBAW]::Default() }

            $channelData = Get-ChannelData `
                -ChannelGroupIDs $ChannelGroups[$zoneIndex] `
                -FixtureChannelIDs $FixtureChannels[$zoneIndex] `
                -ColorValues $activeColorValues `
                -DisableAmber:$DisableAmber `
                -DisableWhite:$DisableWhite

            [void]$channelGroupData.AddRange($channelData.ChannelGroupData)
            [void]$fixtureChannelData.AddRange($channelData.fixtureChannelData)
        }
    }

    return @{
        Name = "$functionName"
        ChannelGroupData = $channelGroupData
        FixtureChannelData = $fixtureChannelData
    }
}

function Get-HorizontalZones {
    param (
        [Parameter(Mandatory = $true)]
        [int]$NumberOfZones,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$ChannelGroups,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$FixtureChannels,

        [Parameter(Mandatory = $true)]
        [RGBAW]$ColorValues,

        [switch]$DisableAmber = $false,
    
        [switch]$DisableWhite = $false,

        [Parameter(Mandatory = $true)]
        [int]$Rows,

        [switch]$RowsSnake,
        
        [switch]$Left,
        [switch]$Right,
        [switch]$Center,
        [switch]$Outer
    )

    if (-not ($Left -or $Right -or $Center -or $Outer)) {
        throw "One of -Left, -Right, -Center, or -Outer must be selected!"
    }

    $zonesPerRow = [math]::Ceiling($NumberOfZones / $Rows)

    $functionName = if ($Left) {
        "Left"
        $activeColumnTrue = $zonesPerRow
        $activeColumnFalse = 1
    } elseif ($Right) {
        "Right"
        $activeColumnTrue = 1
        $activeColumnFalse = $zonesPerRow
    } elseif ($Center) {
        "Center"
    } elseif ($Outer) {
        "HOuter"
    }

    $channelGroupData = [System.Collections.ArrayList]@()
    $fixtureChannelData = [System.Collections.ArrayList]@()

    for ($row = 1; $row -le $Rows; $row++) {
        $activeColumn = if (-not $Center) {
            if ($RowsSnake -and ($row % 2 -eq 0)) { $activeColumnTrue } else { $activeColumnFalse }
        }

        for ($col = 1; $col -le $zonesPerRow; $col++) {
            $zoneIndex = ($row - 1) * $zonesPerRow + $col - 1
            
            $activeZone = if ($Center) {
                if ($zonesPerRow % 2 -eq 0) {
                    # If $zonesPerRow is even, activate the two center zones
                    ($col -eq ($zonesPerRow / 2) -or $col -eq ($zonesPerRow / 2) + 1)
                } else {
                    # If $zonesPerRow is odd, activate only the center zone
                    ($col -eq [math]::Ceiling($zonesPerRow / 2))
                }
            } elseif ($Outer) {
                # If the first or last zone in a row.
                ($col -in 1, $zonesPerRow)
            } else {
                # Left or Right zone as determined per row.
                ($col -eq $activeColumn)
            }

            $activeColorValues = if ($activeZone) { $ColorValues } else { [RGBAW]::Default() }

            $channelData = Get-ChannelData `
                -ChannelGroupIDs $ChannelGroups[$zoneIndex] `
                -FixtureChannelIDs $FixtureChannels[$zoneIndex] `
                -ColorValues $activeColorValues `
                -DisableAmber:$DisableAmber `
                -DisableWhite:$DisableWhite

            [void]$channelGroupData.AddRange($channelData.ChannelGroupData)
            [void]$fixtureChannelData.AddRange($channelData.fixtureChannelData)
        }
    }

    return @{
        Name = "$functionName"
        ChannelGroupData = $channelGroupData
        FixtureChannelData = $fixtureChannelData
    }
}

function Get-StrobeColors {
    param (
        [Parameter(Mandatory = $true)]
        [int]$NumberOfZones,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$ChannelGroups,

        [Parameter(Mandatory = $true)]
        [RGBAW[]]$FixtureChannels,

        [Parameter(Mandatory = $true)]
        [RGBAW]$ColorValues,

        [switch]$DisableAmber = $false,
    
        [switch]$DisableWhite = $false,

        [Parameter(Mandatory = $true)]
        [int]$StrobeChannelGroupID,

        [Parameter(Mandatory = $true)]
        [int]$StrobeFixtureChannelID,

        [Parameter(Mandatory = $true)]
        [int]$Frequency,

        [Parameter(Mandatory = $true)]
        [int]$MaxFrequency
    )
    
    $channelGroupData = [System.Collections.ArrayList]@()
    $fixtureChannelData = [System.Collections.ArrayList]@()
    $StrobeValue = Get-DMXFromHz -Frequency $Frequency -MaxFrequency $MaxFrequency

    for ($currentZone = 0; $currentZone -lt $NumberOfZones; $currentZone++) {

        $channelData = Get-ChannelData `
            -ChannelGroupIDs $ChannelGroups[$currentZone] `
            -FixtureChannelIDs $FixtureChannels[$currentZone] `
            -ColorValues $ColorValues `
            -DisableAmber:$DisableAmber `
            -DisableWhite:$DisableWhite

        [void]$channelGroupData.AddRange($channelData.ChannelGroupData)
        [void]$fixtureChannelData.AddRange($channelData.fixtureChannelData)
    }

    [void]$channelGroupData.Add($StrobeChannelGroupID)
    [void]$channelGroupData.Add($StrobeValue)
    [void]$fixtureChannelData.Add($StrobeFixtureChannelID)
    [void]$fixtureChannelData.Add($StrobeValue)

    $zoneData = @{
        Name = "Strobe"
        ChannelGroupData = $channelGroupData
        FixtureChannelData = $fixtureChannelData
    }

    return New-Object PSObject -Property $zoneData
}

function Get-StrobeHz {
    param (
        [Parameter(Mandatory = $true)]
        [int]$ChannelGroupID,

        [Parameter(Mandatory = $true)]
        [int]$FixtureChannelID,

        [Parameter(Mandatory = $true)]
        [int]$Frequency,

        [Parameter(Mandatory = $true)]
        [int]$MaxFrequency,

        [int]$StrobeValueSlow,
        [int]$StrobeValueMedium,
        [int]$StrobeValueFast
    )
    
        $channelGroupData = [System.Collections.ArrayList]@()
        $fixtureChannelData = [System.Collections.ArrayList]@()
        $channelLevel = Get-DMXFromHz -Frequency $Frequency -MaxFrequency $MaxFrequency

        [void]$channelGroupData.Add($ChannelGroupID)
        [void]$channelGroupData.Add($channelLevel)
        [void]$fixtureChannelData.Add($FixtureChannelID)
        [void]$fixtureChannelData.Add($channelLevel)

        return @{
            Name = "Strobe $("{0:D2}" -f $Frequency)Hz"
            ChannelGroupData = $channelGroupData
            FixtureChannelData = $fixtureChannelData
        }
}

function Get-UVScene {
    param (
        [Parameter(Mandatory = $true)]
        [array]$UVChannelGroupIDs,

        [Parameter(Mandatory = $true)]
        [array]$UVFixtureChannels
    )
    
        $channelGroupData = [System.Collections.ArrayList]@()
        $fixtureChannelData = [System.Collections.ArrayList]@()

        for ($zone = 0; $zone -lt $UVChannelGroupIDs.Count; $zone++) {
            [void]$channelGroupData.Add($UVChannelGroupIDs[$zone])
            [void]$channelGroupData.Add(255)
            [void]$fixtureChannelData.Add($UVFixtureChannels[$zone])
            [void]$fixtureChannelData.Add(255)
        }

        return @{
            Name = "FULL"
            ChannelGroupData = $channelGroupData
            FixtureChannelData = $fixtureChannelData
        }
}
#endregion

# All Strobe Frequencies
if ($StrobeMaxHz -gt 0) {
    for ($currentHz = 1; $currentHz -le $StrobeMaxHz; $currentHz++) {
        $function = Get-StrobeHz -ChannelGroupID $StrobeChannelGroupID -FixtureChannelID $StrobeFixtureChannel -Frequency $currentHz -MaxFrequency $StrobeMaxHz
        $sceneName = "$FixturePrefix - $($function.Name)"
        if ($StrobeValueSlow -eq $currentHz) {
            $sceneName += " - Slow"
        }
        if ($StrobeValueMedium -eq $currentHz) {
            $sceneName += " - Medium"
        }
        if ($StrobeValueFast -eq $currentHz) {
            $sceneName += " - Fast"
        }
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName $sceneName `
            -FunctionPath "$FunctionPathPrefix/Strobe" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData

        $FunctionID++
    }
}

#region Special Strobe Frequencies
# if ($null -ne $StrobeValueSlow) {
#     $function = Get-StrobeHz -ChannelGroupID $StrobeChannelGroupID -FixtureChannelID $StrobeFixtureChannel -Frequency $StrobeValueSlow -MaxFrequency $StrobeMaxHz
#     $functions += Get-SceneObject `
#         -FunctionID $FunctionID `
#         -sceneName "$FixturePrefix - $($function.Name) [Slow]" `
#         -FunctionPath "$FunctionPathPrefix/Strobe" `
#         -channelGroupsData $function.ChannelGroupData `
#         -FixtureIDs $FixtureIDs `
#         -FixtureChannelPairs $function.FixtureChannelData
#
#     $FunctionID++
# }
# if ($null -ne $StrobeValueMedium) {
#     $function = Get-StrobeHz -ChannelGroupID $StrobeChannelGroupID -FixtureChannelID $StrobeFixtureChannel -Frequency $StrobeValueMedium -MaxFrequency $StrobeMaxHz
#     $functions += Get-SceneObject `
#         -FunctionID $FunctionID `
#         -sceneName "$FixturePrefix - $($function.Name) [Medium]" `
#         -FunctionPath "$FunctionPathPrefix/Strobe" `
#         -channelGroupsData $function.ChannelGroupData `
#         -FixtureIDs $FixtureIDs `
#         -FixtureChannelPairs $function.FixtureChannelData
#
#     $FunctionID++
# }
# if ($null -ne $StrobeValueFast) {
#     $function = Get-StrobeHz -ChannelGroupID $StrobeChannelGroupID -FixtureChannelID $StrobeFixtureChannel -Frequency $StrobeValueFast -MaxFrequency $StrobeMaxHz
#     $functions += Get-SceneObject `
#         -FunctionID $FunctionID `
#         -sceneName "$FixturePrefix - $($function.Name) [Fast]" `
#         -FunctionPath "$FunctionPathPrefix/Strobe" `
#         -channelGroupsData $function.ChannelGroupData `
#         -FixtureIDs $FixtureIDs `
#         -FixtureChannelPairs $function.FixtureChannelData
#
#     $FunctionID++
# }
#endregion

class ColorFunctions {
    [string]$Name
    [System.Collections.ArrayList]$FunctionIDs

    [void] Add([int]$FunctionID) {
        [void]$this.FunctionIDs.Add($FunctionID)
    }

    ColorFunctions([string]$ColorName) {
        $this.Name = $ColorName
        $this.FunctionIDs = [System.Collections.ArrayList]@()
    }
}

class ChaseCollection {
    [System.Collections.ArrayList]$Full
    [System.Collections.ArrayList]$Zone
    [System.Collections.ArrayList]$Wave
    [System.Collections.ArrayList]$Row
    [System.Collections.ArrayList]$TopBottom
    [System.Collections.ArrayList]$MidOuter
    [System.Collections.ArrayList]$Column
    [System.Collections.ArrayList]$RightLeft
    [System.Collections.ArrayList]$CenterOuter
    [System.Collections.ArrayList]$Alternate

    [void] AddFull([ColorFunctions]$functionCollection) {
        [void]$this.Full.Add($functionCollection)
    }

    [void] AddZone([ColorFunctions]$functionCollection) {
        [void]$this.Zone.Add($functionCollection)
    }

    [void] AddWave([ColorFunctions]$functionCollection) {
        [void]$this.Wave.Add($functionCollection)
    }

    [void] AddRow([ColorFunctions]$functionCollection) {
        [void]$this.Row.Add($functionCollection)
    }

    [void] AddTopBottom([ColorFunctions]$functionCollection) {
        [void]$this.Row.Add($functionCollection)
        [void]$this.TopBottom.Add($functionCollection)
    }

    [void] AddMidOuter([ColorFunctions]$functionCollection) {
        [void]$this.Row.Add($functionCollection)
        [void]$this.MidOuter.Add($functionCollection)
    }

    [void] AddColumn([ColorFunctions]$functionCollection) {
        [void]$this.Column.Add($functionCollection)
    }

    [void] AddRightLeft([ColorFunctions]$functionCollection) {
        [void]$this.Column.Add($functionCollection)
        [void]$this.RightLeft.Add($functionCollection)
    }

    [void] AddCenterOuter([ColorFunctions]$functionCollection) {
        [void]$this.Column.Add($functionCollection)
        [void]$this.CenterOuter.Add($functionCollection)
    }

    [void] AddAlternate([ColorFunctions]$functionCollection) {
        [void]$this.Alternate.Add($functionCollection)
    }

    [System.Collections.ArrayList] GetAllArrays() {
        $result = New-Object System.Collections.ArrayList
        $result.AddRange($this.Full)
        $result.AddRange($this.Zone)
        $result.AddRange($this.Wave)
        $result.AddRange($this.Row)
        $result.AddRange($this.Column)
        $result.AddRange($this.Alternate)
        return $result
    }

    ChaseCollection() {
        $this.Full = [System.Collections.ArrayList]@()
        $this.Zone = [System.Collections.ArrayList]@()
        $this.Wave = [System.Collections.ArrayList]@()
        $this.Row = [System.Collections.ArrayList]@()
        $this.TopBottom = [System.Collections.ArrayList]@()
        $this.MidOuter = [System.Collections.ArrayList]@()
        $this.Column = [System.Collections.ArrayList]@()
        $this.RightLeft = [System.Collections.ArrayList]@()
        $this.CenterOuter = [System.Collections.ArrayList]@()
        $this.Alternate = [System.Collections.ArrayList]@()
    }
}

#region Generate Color Scenes
if ($UVChannelGroupIDs -and $UVFixtureChannels) {
    if ($UVChannelGroupIDs.Count -ne $UVFixtureChannels.Count) {
        throw "UVChannelGroupIDs and UVFixtureChannels must have the same number of channel IDs."
    }
    
    $function = Get-UVScene -UVChannelGroupIDs $UVChannelGroupIDs -UVFixtureChannels $UVFixtureChannels
    $functions += Get-SceneObject `
        -FunctionID $FunctionID `
        -sceneName "$FixturePrefix - UV" `
        -FunctionPath "$FunctionPathPrefix" `
        -channelGroupsData $function.ChannelGroupData `
        -FixtureIDs $FixtureIDs `
        -FixtureChannelPairs $function.FixtureChannelData

    $FunctionID++
}

$ChaseScenes = [ChaseCollection]::new()

$params = @{
    NumberOfZones = $NumberOfZones
    ChannelGroups = $ChannelGroups
    FixtureChannels = $FixtureChannels
    ColorValues = [RGBAW]::Default
    DisableAmber = $DisableAmber
    DisableWhite = $DisableWhite
}

# Blackout
$params.ColorValues = [RGBAW]::Default()
$function = Get-AllZones @params
$functions += Get-SceneObject `
    -FunctionID $FunctionID `
    -sceneName "$FixturePrefix - Blackout" `
    -FunctionPath $FunctionPathPrefix `
    -channelGroupsData $function.ChannelGroupData `
    -FixtureIDs $FixtureIDs `
    -FixtureChannelPairs $function.FixtureChannelData
$FunctionID++

# The CSV Headers are as follows - ID,Name,Red,Green,Blue,Amber,White
foreach ($color in $colors) {

    # Combine Function Path Suffix from the 1st and 2nd columns
    $colorName = "$($color.ID) $($color.Name)"
    if ($NumberOfZones -eq 1) {
        $colorFunctionPath = "$FunctionPathPrefix"
    } else {
        $colorFunctionPath = "$FunctionPathPrefix/$colorName"
    }

    $fullFunctions = [ColorFunctions]::new($colorName)
    $zoneFunctions = [ColorFunctions]::new($colorName)
    $waveFunctions = [ColorFunctions]::new($colorName)
    $rowTBFunctions = [ColorFunctions]::new($colorName)
    $rowMOFunctions = [ColorFunctions]::new($colorName)
    $columnRLFunctions = [ColorFunctions]::new($colorName)
    $columnCOFunctions = [ColorFunctions]::new($colorName)
    $alternateFunctions = [ColorFunctions]::new($colorName)

    $colorData = [RGBAW]::new(
        $color.Red,
        $color.Green,
        $color.Blue,
        $color.Amber,
        $color.White
    )

    if ($DisableAmber -and ($colorData.Amber -gt 0)) {
        Write-Output "Skipping [$($colorName)] because it has Amber [$($colorData.Amber)]"
        continue
    }

    if ($DisableWhite -and ($colorData.White -gt 0)) {
        Write-Output "Skipping [$($colorName)] because it has White [$($colorData.White)]"
        continue
    }

    $params.ColorValues = $colorData
    
    $function = Get-AllZones @params
    $functions += Get-SceneObject `
        -FunctionID $FunctionID `
        -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
        -FunctionPath "$colorFunctionPath" `
        -channelGroupsData $function.ChannelGroupData `
        -FixtureIDs $FixtureIDs `
        -FixtureChannelPairs $function.FixtureChannelData
    $fullFunctions.Add($FunctionID)
    $FunctionID++

    # Unfortunately this function creates an astronomical number of functions once you get above 3 zones.
    # 2^ZONENUMBER * NUMBEROFCOLORS
    # For example, if your light has 3 zones with 40 colors 2^3*40 = 320 functions
    # If your light has 6 zones with 40 colors 2^6*40 = 2560 functions
    # $functionData = Get-ZoneCombinations -ColorValues $colorData @params
    # foreach ($function in $functionData) {
    #     $functions += Get-SceneObject `
    #         -FunctionID $FunctionID `
    #         -sceneName "$FixturePrefix - $($colorName)$($(if ($NumberOfZones -ne 1) { " [$($function.Name)]" }))" `
    #         -FunctionPath "$colorFunctionPath/Combinations" `
    #         -channelGroupsData $function.ChannelGroupData `
    #         -FixtureIDs $FixtureIDs `
    #         -FixtureChannelPairs $function.FixtureChannelData
    #     $zoneFunctions.Add($FunctionID)
    #
    #     $FunctionID++
    # }
    
    if ($NumberOfZones -gt 1) {
        $functionData = Get-IndividualZones @params
        foreach ($function in $functionData) {
            $functions += Get-SceneObject `
                -FunctionID $FunctionID `
                -sceneName "$FixturePrefix - $($colorName)$($(if ($NumberOfZones -ne 1) { " [$($function.Name)]" }))" `
                -FunctionPath "$colorFunctionPath/Zones" `
                -channelGroupsData $function.ChannelGroupData `
                -FixtureIDs $FixtureIDs `
                -FixtureChannelPairs $function.FixtureChannelData
            $zoneFunctions.Add($FunctionID)
            $FunctionID++
        }

        $functionData = Get-ZoneWave @params
        foreach ($function in $functionData) {
            $functions += Get-SceneObject `
                -FunctionID $FunctionID `
                -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
                -FunctionPath "$colorFunctionPath/Wave" `
                -channelGroupsData $function.ChannelGroupData `
                -FixtureIDs $FixtureIDs `
                -FixtureChannelPairs $function.FixtureChannelData
            $waveFunctions.Add($FunctionID)
            $FunctionID++
        }
    }

    # Odd and Even Zones
    if ($NumberOfZones -gt 2) {
        $function = Get-AlternateZones -Even:$false @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $alternateFunctions.Add($FunctionID)
        $FunctionID++
    
        $function = Get-AlternateZones -Even:$true @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $alternateFunctions.Add($FunctionID)
        $FunctionID++
    }
        
    # Horizontal "Left" and "Right" Zones
    if ($NumberOfZones/$NumberOfRows -gt 1) {
        $function = Get-HorizontalZones -Rows $NumberOfRows -RowsSnake:$RowsSnake -Left @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $columnRLFunctions.Add($FunctionID)

        $FunctionID++
        
        $function = Get-HorizontalZones -Rows $NumberOfRows -RowsSnake:$RowsSnake -Right @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $columnRLFunctions.Add($FunctionID)

        $FunctionID++
    }

    # Horizontal "Center" and "Outer" Zones
    if ($NumberOfZones/$NumberOfRows -gt 2) {
        $function = Get-HorizontalZones -Rows $NumberOfRows -RowsSnake:$RowsSnake -Center @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $columnCOFunctions.Add($FunctionID)

        $FunctionID++
        
        $function = Get-HorizontalZones -Rows $NumberOfRows -RowsSnake:$RowsSnake -Outer @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $columnCOFunctions.Add($FunctionID)

        $FunctionID++
    }
    
    # Vertical "Top" and "Bottom" Zones
    if ($NumberOfRows -gt 1) {
        $function = Get-VerticalZones -Rows $NumberOfRows -Top @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $rowTBFunctions.Add($FunctionID)

        $FunctionID++
        
        $function = Get-VerticalZones -Rows $NumberOfRows -Bottom @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $rowTBFunctions.Add($FunctionID)

        $FunctionID++
    }

    # Vertical "Middle" Zones
    if ($NumberOfRows -gt 2) {
        $function = Get-VerticalZones -Rows $NumberOfRows -Middle @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $rowMOFunctions.Add($FunctionID)
        $FunctionID++

        $function = Get-VerticalZones -Rows $NumberOfRows -Outer @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name)]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
        $rowMOFunctions.Add($FunctionID)
        $FunctionID++
    }

    # Special Strobe Frequencies
    if ($StrobeEnable) {
        $function = Get-StrobeColors -StrobeChannelGroupID $StrobeChannelGroupID -StrobeFixtureChannelID $StrobeFixtureChannel -Frequency $StrobeValueSlow -MaxFrequency $StrobeMaxHz @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name) Slow]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
    
        $FunctionID++
    }
    if ($StrobeEnable) {
        $function = Get-StrobeColors -StrobeChannelGroupID $StrobeChannelGroupID -StrobeFixtureChannelID $StrobeFixtureChannel -Frequency $StrobeValueMedium -MaxFrequency $StrobeMaxHz @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name) Medium]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
    
        $FunctionID++
    }
    if ($StrobeEnable) {
        $function = Get-StrobeColors -StrobeChannelGroupID $StrobeChannelGroupID -StrobeFixtureChannelID $StrobeFixtureChannel -Frequency $StrobeValueFast -MaxFrequency $StrobeMaxHz @params
        $functions += Get-SceneObject `
            -FunctionID $FunctionID `
            -sceneName "$FixturePrefix - $($colorName) [$($function.Name) Fast]" `
            -FunctionPath "$colorFunctionPath" `
            -channelGroupsData $function.ChannelGroupData `
            -FixtureIDs $FixtureIDs `
            -FixtureChannelPairs $function.FixtureChannelData
    
        $FunctionID++
    }

    Write-Verbose "$($colorName) Full Scenes: $($fullFunctions.FunctionIDs.Count)"
    $ChaseScenes.AddFull($fullFunctions)
    Write-Verbose "$($colorName) Zone Scenes: $($zoneFunctions.FunctionIDs.Count)"
    $ChaseScenes.AddZone($zoneFunctions)
    Write-Verbose "$($colorName) Wave Scenes: $($waveFunctions.FunctionIDs.Count)"
    $ChaseScenes.AddWave($waveFunctions)
    Write-Verbose "$($colorName) Row Top/Bottom Scenes: $($rowTBFunctions.FunctionIDs.Count)"
    $ChaseScenes.AddTopBottom($rowTBFunctions)
    Write-Verbose "$($colorName) Row Middle/Outer Scenes: $($rowMOFunctions.FunctionIDs.Count)"
    $ChaseScenes.AddMidOuter($rowMOFunctions)
    Write-Verbose "$($colorName) Column Right/Left Scenes: $($columnRLFunctions.FunctionIDs.Count)"
    $ChaseScenes.AddRightLeft($columnRLFunctions)
    Write-Verbose "$($colorName) Column Center/Outer Scenes: $($columnCOFunctions.FunctionIDs.Count)"
    $ChaseScenes.AddCenterOuter($columnCOFunctions)
    Write-Verbose "$($colorName) Alternate Scenes: $($alternateFunctions.FunctionIDs.Count)"
    $ChaseScenes.AddAlternate($alternateFunctions)
}
#endregion

#region Generate Color Component Chases
Write-Host "Color Component Chases"
$FunctionID = ($FixtureGroup * $FunctionMultiplier + 10000)
$ComponentChases = [ChaseCollection]::new()

# Full Chases
# foreach ($color in $ChaseScenes.Full) {
#     $fullChases = [ColorFunctions]::new($color.Name)
#     $functions += Get-ChaseObject `
#         -FunctionID $FunctionID `
#         -ChaseName "$FixturePrefix - $($color.Name) - Full" `
#         -FunctionPath "$FunctionPathPrefix/Color Components/$($color.Name)" `
#         -Direction "Forward" `
#         -RunOrder "Loop" `
#         -Steps @([Step]::new($color.FunctionIDs[0]))
#     $fullChases.Add($FunctionID)
#     $ComponentChases.AddFull($fullChases)
#     $FunctionID++
# }

if ($NumberOfZones -gt 1) {
    # Zone Chases
    foreach ($color in $ChaseScenes.Zone) {
        if ($color.FunctionIDs.Count -eq 0) {
            continue
        }
        $zoneChases = [ColorFunctions]::new($color.Name)
        $steps = [System.Collections.ArrayList]@()
        foreach ($sceneID in $color.FunctionIDs) {
            [void]$steps.Add([Step]::new($sceneID))
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - $($color.Name) - Zones" `
            -ChasePath "$FunctionPathPrefix/Color Components/$($color.Name)" `
            -Steps $steps
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            [void]$zoneChases.Add(($styles.NextID - $funcIndex))
        }
        $ComponentChases.AddZone($zoneChases)
    }

    # Wave Chases
    foreach ($color in $ChaseScenes.Wave) {
        if ($color.FunctionIDs.Count -eq 0) {
            continue
        }
        $waveChases = [ColorFunctions]::new($color.Name)
        $steps = [System.Collections.ArrayList]@()
        foreach ($sceneID in $color.FunctionIDs) {
            [void]$steps.Add([Step]::new($sceneID))
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - $($color.Name) - Waves" `
            -ChasePath "$FunctionPathPrefix/Color Components/$($color.Name)" `
            -Steps $steps
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $waveChases.Add(($styles.NextID - $funcIndex))
        }
        $ComponentChases.AddWave($waveChases)
    }
    
    # Row Chases - Top & Bottom
    foreach ($color in $ChaseScenes.TopBottom) {
        if ($color.FunctionIDs.Count -eq 0) {
            continue
        }
        $rowChases = [ColorFunctions]::new($color.Name)
        $steps = [System.Collections.ArrayList]@()
        foreach ($sceneID in $color.FunctionIDs) {
            [void]$steps.Add([Step]::new($sceneID))
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - $($color.Name) - Row [Top/Bottom]" `
            -ChasePath "$FunctionPathPrefix/Color Components/$($color.Name)" `
            -Steps $steps
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $rowChases.Add(($styles.NextID - $funcIndex))
        }
        $ComponentChases.AddTopBottom($rowChases)
    }

    # Row Chases - Middle & Outer
    foreach ($color in $ChaseScenes.MidOuter) {
        if ($color.FunctionIDs.Count -eq 0) {
            continue
        }
        $rowChases = [ColorFunctions]::new($color.Name)
        $steps = [System.Collections.ArrayList]@()
        foreach ($sceneID in $color.FunctionIDs) {
            [void]$steps.Add([Step]::new($sceneID))
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - $($color.Name) - Row [Mid/Outer]" `
            -ChasePath "$FunctionPathPrefix/Color Components/$($color.Name)" `
            -Steps $steps
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $rowChases.Add(($styles.NextID - $funcIndex))
        }
        $ComponentChases.AddMidOuter($rowChases)
    }

    # Alternate Chases - Even & Odd
    foreach ($color in $ChaseScenes.Alternate) {
        if ($color.FunctionIDs.Count -eq 0) {
            continue
        }
        $alternateChases = [ColorFunctions]::new($color.Name)
        $steps = [System.Collections.ArrayList]@()
        foreach ($sceneID in $color.FunctionIDs) {
            [void]$steps.Add([Step]::new($sceneID))
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - $($color.Name) - Alternate [Even/Odd]" `
            -ChasePath "$FunctionPathPrefix/Color Components/$($color.Name)" `
            -Steps $steps
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $alternateChases.Add(($styles.NextID - $funcIndex))
        }
        $ComponentChases.AddAlternate($alternateChases)
    }

    # Column Chases - Right & Left
    foreach ($color in $ChaseScenes.RightLeft) {
        if ($color.FunctionIDs.Count -eq 0) {
            continue
        }
        $columnChases = [ColorFunctions]::new($color.Name)
        $steps = [System.Collections.ArrayList]@()
        foreach ($sceneID in $color.FunctionIDs) {
            [void]$steps.Add([Step]::new($sceneID))
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - $($color.Name) - Column [Right/Left]" `
            -ChasePath "$FunctionPathPrefix/Color Components/$($color.Name)" `
            -Steps $steps
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $columnChases.Add(($styles.NextID - $funcIndex))
        }
        $ComponentChases.AddRightLeft($columnChases)
    }
    
    # Column Chases - Center & Outer
    foreach ($color in $ChaseScenes.CenterOuter) {
        if ($color.FunctionIDs.Count -eq 0) {
            continue
        }
        $columnChases = [ColorFunctions]::new($color.Name)
        $steps = [System.Collections.ArrayList]@()
        foreach ($sceneID in $color.FunctionIDs) {
            [void]$steps.Add([Step]::new($sceneID))
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - $($color.Name) - Column [Center/Outer]" `
            -ChasePath "$FunctionPathPrefix/Color Components/$($color.Name)" `
            -Steps $steps
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $columnChases.Add(($styles.NextID - $funcIndex))
        }
        $ComponentChases.AddCenterOuter($columnChases)
    }
}
#endregion

#region Generate Sub-Master Chases
Write-Host "Sub-Master Chases"
$FunctionID = ($FixtureGroup * $FunctionMultiplier + 20000)
$SubMasterChases = [ChaseCollection]::new()

# Full Chases
$fullChases = [ColorFunctions]::new("Full")
$steps = [System.Collections.ArrayList]@()
# foreach ($color in $ComponentChases.Full) {
#     foreach ($stepID in $color.FunctionIDs) {
#         [void]$steps.Add([Step]::new($stepID))
#     }
# }
foreach ($color in $ChaseScenes.Full) {
    foreach ($stepID in $color.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
$styles = Get-ChaseStyles `
    -FunctionID $FunctionID `
    -NamePrefix "$FixturePrefix - Full" `
    -ChasePath "$FunctionPathPrefix/SubMaster" `
    -Steps $steps `
    -Loop `
    -PingPong `
    -Random
$functions += $styles.Functions
$FunctionID = $styles.NextID
for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
    $fullChases.Add(($styles.NextID - $funcIndex))
}
$SubMasterChases.AddFull($fullChases)

if ($NumberOfZones -gt 1) {
    # Zone Chases
    if ($ComponentChases.Zone.Count -gt 0) {
        $zoneChases = [ColorFunctions]::new("Zone")
        $steps = [System.Collections.ArrayList]@()
        foreach ($color in $ComponentChases.Zone) {
            foreach ($stepID in $color.FunctionIDs) {
                [void]$steps.Add([Step]::new($stepID))
            }
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - Zone" `
            -ChasePath "$FunctionPathPrefix/SubMaster" `
            -Steps $steps `
            -Loop `
            -PingPong `
            -Random
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $zoneChases.Add(($styles.NextID - $funcIndex))
        }
        $SubMasterChases.AddZone($zoneChases)
    }

    # Wave Chases
    if ($ComponentChases.Wave.Count -gt 0) {
        $waveChases = [ColorFunctions]::new("Wave")
        $steps = [System.Collections.ArrayList]@()
        foreach ($color in $ComponentChases.Wave) {
            foreach ($stepID in $color.FunctionIDs) {
                [void]$steps.Add([Step]::new($stepID))
            }
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - Wave" `
            -ChasePath "$FunctionPathPrefix/SubMaster" `
            -Steps $steps `
            -Loop `
            -PingPong `
            -Random
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $waveChases.Add(($styles.NextID - $funcIndex))
        }
        $SubMasterChases.AddWave($waveChases)
    }

    # Row Chases - Top & Bottom
    if ($ComponentChases.TopBottom.Count -gt 0) {
        $rowChases = [ColorFunctions]::new("Top/Bottom")
        $steps = [System.Collections.ArrayList]@()
        foreach ($color in $ComponentChases.TopBottom) {
            foreach ($stepID in $color.FunctionIDs) {
                [void]$steps.Add([Step]::new($stepID))
            }
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - Row [Top/Bottom]" `
            -ChasePath "$FunctionPathPrefix/SubMaster" `
            -Steps $steps `
            -Random
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $rowChases.Add(($styles.NextID - $funcIndex))
        }
        $SubMasterChases.AddTopBottom($rowChases)
    }

    # Row Chases - Middle & Outer
    if ($ComponentChases.MidOuter.Count -gt 0) {
        $rowChases = [ColorFunctions]::new("Middle/Outer")
        $steps = [System.Collections.ArrayList]@()
        foreach ($color in $ComponentChases.MidOuter) {
            foreach ($stepID in $color.FunctionIDs) {
                [void]$steps.Add([Step]::new($stepID))
            }
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - Row [Middle/Outer]" `
            -ChasePath "$FunctionPathPrefix/SubMaster" `
            -Steps $steps `
            -Random
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $rowChases.Add(($styles.NextID - $funcIndex))
        }
        $SubMasterChases.AddMidOuter($rowChases)
    }

    # Column Chases - Right & Left
    if ($ComponentChases.RightLeft.Count -gt 0) {
        $columnChases = [ColorFunctions]::new("Right/Left")
        $steps = [System.Collections.ArrayList]@()
        foreach ($color in $ComponentChases.RightLeft) {
            foreach ($stepID in $color.FunctionIDs) {
                [void]$steps.Add([Step]::new($stepID))
            }
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - Column [Right/Left]" `
            -ChasePath "$FunctionPathPrefix/SubMaster" `
            -Steps $steps `
            -Random
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $columnChases.Add(($styles.NextID - $funcIndex))
        }
        $SubMasterChases.AddRightLeft($columnChases)
    }

    # Column Chases - Center & Outer
    if ($ComponentChases.CenterOuter.Count -gt 0) {
        $columnChases = [ColorFunctions]::new("Center/Outer")
        $steps = [System.Collections.ArrayList]@()
        foreach ($color in $ComponentChases.CenterOuter) {
            foreach ($stepID in $color.FunctionIDs) {
                [void]$steps.Add([Step]::new($stepID))
            }
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - Column [Center/Outer]" `
            -ChasePath "$FunctionPathPrefix/SubMaster" `
            -Steps $steps `
            -Random
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $columnChases.Add(($styles.NextID - $funcIndex))
        }
        $SubMasterChases.AddCenterOuter($columnChases)
    }

    # Alternate Chases
    if ($ComponentChases.Alternate.Count -gt 0) {
        $alternateChases = [ColorFunctions]::new("Alternate")
        $steps = [System.Collections.ArrayList]@()
        foreach ($color in $ComponentChases.Alternate) {
            foreach ($stepID in $color.FunctionIDs) {
                [void]$steps.Add([Step]::new($stepID))
            }
        }
        $styles = Get-ChaseStyles `
            -FunctionID $FunctionID `
            -NamePrefix "$FixturePrefix - Alternate" `
            -ChasePath "$FunctionPathPrefix/SubMaster" `
            -Steps $steps `
            -Random
        $functions += $styles.Functions
        $FunctionID = $styles.NextID
        for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
            $alternateChases.Add(($styles.NextID - $funcIndex))
        }
        $SubMasterChases.AddAlternate($alternateChases)
    }
}
#endregion

#region Generate Master Chases
Write-Host "Master Chases"
$FunctionID = ($FixtureGroup * $FunctionMultiplier + 30000)

# All Chase Functions
$MasterGridChases = [ColorFunctions]::new("Grid")
$steps = [System.Collections.ArrayList]@()
foreach ($chase in $SubMasterChases.TopBottom) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
foreach ($chase in $SubMasterChases.MidOuter) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
foreach ($chase in $SubMasterChases.Alternate) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
foreach ($chase in $SubMasterChases.RightLeft) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
foreach ($chase in $SubMasterChases.CenterOuter) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
if ($steps.Count -gt 1) {
    $styles = Get-ChaseStyles `
        -FunctionID $FunctionID `
        -NamePrefix "$FixturePrefix - Grid" `
        -ChasePath "$FunctionPathPrefix/Master" `
        -Steps $steps `
        -Loop `
        -Random
    $functions += $styles.Functions
    $FunctionID = $styles.NextID
    for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
        $MasterGridChases.Add(($styles.NextID - $funcIndex))
    }
}

# Row Chase Functions (TOP/BOTTOM - TOP&BOTTOM/MIDDLE - EVEN/ODD)
$MasterRowChases = [ColorFunctions]::new("Row")
$steps = [System.Collections.ArrayList]@()
foreach ($chase in $SubMasterChases.TopBottom) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
foreach ($chase in $SubMasterChases.MidOuter) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
foreach ($chase in $SubMasterChases.Alternate) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
if ($steps.Count -gt 1) {
    $styles = Get-ChaseStyles `
        -FunctionID $FunctionID `
        -NamePrefix "$FixturePrefix - Row" `
        -ChasePath "$FunctionPathPrefix/Master" `
        -Steps $steps
    $functions += $styles.Functions
    $FunctionID = $styles.NextID
    for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
        $MasterRowChases.Add(($styles.NextID - $funcIndex))
    }
}

# Column Chase Functions (RIGHT/LEFT - CENTER/OUTER)
$MasterColumnChases = [ColorFunctions]::new("Column")
$steps = [System.Collections.ArrayList]@()
foreach ($chase in $SubMasterChases.RightLeft) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
foreach ($chase in $SubMasterChases.CenterOuter) {
    foreach ($stepID in $chase.FunctionIDs) {
        [void]$steps.Add([Step]::new($stepID))
    }
}
if ($steps.Count -gt 1) {
    $styles = Get-ChaseStyles `
        -FunctionID $FunctionID `
        -NamePrefix "$FixturePrefix - Column" `
        -ChasePath "$FunctionPathPrefix/Master" `
        -Steps $steps
    $functions += $styles.Functions
    $FunctionID = $styles.NextID
    for ($funcIndex = 1; $funcIndex -le $styles.Functions.Count; $funcIndex++) {
        $MasterColumnChases.Add(($styles.NextID - $funcIndex))
    }
}
#endregion

#region Generate Grand-Master Chases
Write-Host "Grand-Master Chases"
$FunctionID = ($FixtureGroup * $FunctionMultiplier + 40000)
#endregion

#region Save XML
# Specify the base file path where you want to save the XML data
$baseFilePath = "$FixturePrefix.$((Get-Item $CSVFilePath).BaseName)"

# Initialize a counter
$count = 0

# Build the file path with a 2-digit zero-padded number
do {
    $count++
    $xmlFilePath = "{0}{1:D2}.xml" -f $baseFilePath, $count
} while (Test-Path $xmlFilePath)

# Export the array of objects to an XML file
$functions | Set-Content -Path $xmlFilePath

# Write-Output "Created $($FunctionID - $FixtureGroup) Functions!"
#endregion