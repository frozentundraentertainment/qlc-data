# Assuming your script is in the current directory
$scriptPath = ".\Generate-EFXFunctions.ps1"

# Call the script with the parameters
& $scriptPath -CSVFilePath ".\RGBAW_Complete.csv" `
    -FixtureGroup 1 `
    -FixturePrefix "WFXH" `
    -FunctionPathPrefix "Wash FX HEX/Generated" `
    -NumberOfZones 6 `
    -NumberOfRows 2 `
    -RowsSnake:$true `
    -RedChannelGroupIDs   @(16, 22, 28, 34, 40, 46) `
    -GreenChannelGroupIDs @(17, 23, 29, 35, 41, 47) `
    -BlueChannelGroupIDs  @(18, 24, 30, 36, 42, 48) `
    -AmberChannelGroupIDs @(19, 25, 31, 37, 43, 49) `
    -WhiteChannelGroupIDs @(20, 26, 32, 38, 44, 50) `
    -UVChannelGroupIDs    @(21, 27, 33, 39, 45, 51) `
    -StrobeChannelGroupID 9 `
    -FixtureIDs @(0, 1) `
    -RedFixtureChannels   @(4, 10, 16, 22, 28, 34) `
    -GreenFixtureChannels @(5, 11, 17, 23, 29, 35) `
    -BlueFixtureChannels  @(6, 12, 18, 24, 30, 36) `
    -AmberFixtureChannels @(7, 13, 19, 25, 31, 37) `
    -WhiteFixtureChannels @(8, 14, 20, 26, 32, 38) `
    -UVFixtureChannels    @(9, 15, 21, 27, 33, 39) `
    -StrobeFixtureChannel 3 `
    -StrobeValueSlow 15 `
    -StrobeValueMedium 17 `
    -StrobeValueFast 20 `
    -StrobeMaxHz 24
