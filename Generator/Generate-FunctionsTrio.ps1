# Assuming your script is in the current directory
$scriptPath = ".\Generate-EFXFunctions.ps1"

# Call the script with the parameters
& $scriptPath -CSVFilePath ".\RGBAW_Complete.csv" `
    -FixtureGroup 2 `
    -FixturePrefix "Trio" `
    -FunctionPathPrefix "Intimidator Trio/Generated" `
    -NumberOfZones 3 `
    -RedChannelGroupIDs   @(97, 101, 105) `
    -GreenChannelGroupIDs @(98, 102, 106) `
    -BlueChannelGroupIDs  @(99, 103, 107) `
    -DisableAmber `
    -WhiteChannelGroupIDs @(100, 104, 108) `
    -StrobeChannelGroupID 86 `
    -FixtureIDs @(2, 3) `
    -RedFixtureChannels   @(5, 9, 13) `
    -GreenFixtureChannels @(6, 10, 14) `
    -BlueFixtureChannels  @(7, 11, 15) `
    -WhiteFixtureChannels @(8, 12, 16) `
    -StrobeFixtureChannel 25 `
    -StrobeValueSlow 15 `
    -StrobeValueMedium 17 `
    -StrobeValueFast 20 `
    -StrobeMaxHz 20
