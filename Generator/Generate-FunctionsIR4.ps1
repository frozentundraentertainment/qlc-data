# Assuming your script is in the current directory
$scriptPath = ".\Generate-EFXFunctions.ps1"

# Call the script with the parameters
& $scriptPath -CSVFilePath ".\RGBAW_Complete.csv" `
    -FixtureGroup 10 `
    -FixturePrefix "IR4" `
    -FunctionPathPrefix "IR4/ALL/Generated" `
    -NumberOfZones 1 `
    -RedChannelGroupIDs   @(82) `
    -GreenChannelGroupIDs @(83) `
    -BlueChannelGroupIDs  @(84) `
    -AmberChannelGroupIDs @(110) `
    -WhiteChannelGroupIDs @(109) `
    -UVChannelGroupIDs    @(111) `
    -FixtureIDs @(42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73) `
    -RedFixtureChannels   @(0) `
    -GreenFixtureChannels @(1) `
    -BlueFixtureChannels  @(2) `
    -AmberFixtureChannels @(4) `
    -WhiteFixtureChannels @(3) `
    -UVFixtureChannels    @(5)

# Call the script with the parameters
& $scriptPath -CSVFilePath ".\RGBAW_Complete.csv" `
    -FixtureGroup 11 `
    -FixturePrefix "IR4G1" `
    -FunctionPathPrefix "IR4/G1/Generated" `
    -NumberOfZones 1 `
    -RedChannelGroupIDs   @(160) `
    -GreenChannelGroupIDs @(161) `
    -BlueChannelGroupIDs  @(162) `
    -AmberChannelGroupIDs @(164) `
    -WhiteChannelGroupIDs @(163) `
    -UVChannelGroupIDs    @(165) `
    -FixtureIDs @(42, 46, 50, 54, 58, 62, 66, 70) `
    -RedFixtureChannels   @(0) `
    -GreenFixtureChannels @(1) `
    -BlueFixtureChannels  @(2) `
    -AmberFixtureChannels @(4) `
    -WhiteFixtureChannels @(3) `
    -UVFixtureChannels    @(5)

# Call the script with the parameters
& $scriptPath -CSVFilePath ".\RGBAW_Complete.csv" `
    -FixtureGroup 12 `
    -FixturePrefix "IR4G2" `
    -FunctionPathPrefix "IR4/G2/Generated" `
    -NumberOfZones 1 `
    -RedChannelGroupIDs   @(166) `
    -GreenChannelGroupIDs @(167) `
    -BlueChannelGroupIDs  @(168) `
    -AmberChannelGroupIDs @(170) `
    -WhiteChannelGroupIDs @(169) `
    -UVChannelGroupIDs    @(171) `
    -FixtureIDs @(43, 47, 51, 55, 59, 63, 67, 71) `
    -RedFixtureChannels   @(0) `
    -GreenFixtureChannels @(1) `
    -BlueFixtureChannels  @(2) `
    -AmberFixtureChannels @(4) `
    -WhiteFixtureChannels @(3) `
    -UVFixtureChannels    @(5)

# Call the script with the parameters
& $scriptPath -CSVFilePath ".\RGBAW_Complete.csv" `
    -FixtureGroup 13 `
    -FixturePrefix "IR4G3" `
    -FunctionPathPrefix "IR4/G3/Generated" `
    -NumberOfZones 1 `
    -RedChannelGroupIDs   @(172) `
    -GreenChannelGroupIDs @(173) `
    -BlueChannelGroupIDs  @(174) `
    -AmberChannelGroupIDs @(176) `
    -WhiteChannelGroupIDs @(175) `
    -UVChannelGroupIDs    @(177) `
    -FixtureIDs @(44, 48, 52, 56, 60, 64, 68, 72) `
    -RedFixtureChannels   @(0) `
    -GreenFixtureChannels @(1) `
    -BlueFixtureChannels  @(2) `
    -AmberFixtureChannels @(4) `
    -WhiteFixtureChannels @(3) `
    -UVFixtureChannels    @(5)

# Call the script with the parameters
& $scriptPath -CSVFilePath ".\RGBAW_Complete.csv" `
    -FixtureGroup 14 `
    -FixturePrefix "IR4G4" `
    -FunctionPathPrefix "IR4/G4/Generated" `
    -NumberOfZones 1 `
    -RedChannelGroupIDs   @(178) `
    -GreenChannelGroupIDs @(179) `
    -BlueChannelGroupIDs  @(180) `
    -AmberChannelGroupIDs @(182) `
    -WhiteChannelGroupIDs @(181) `
    -UVChannelGroupIDs    @(183) `
    -FixtureIDs @(45, 49, 53, 57, 61, 65, 69, 73) `
    -RedFixtureChannels   @(0) `
    -GreenFixtureChannels @(1) `
    -BlueFixtureChannels  @(2) `
    -AmberFixtureChannels @(4) `
    -WhiteFixtureChannels @(3) `
    -UVFixtureChannels    @(5)
