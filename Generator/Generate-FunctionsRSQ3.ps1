# Assuming your script is in the current directory
$scriptPath = ".\Generate-EFXFunctions.ps1"

# Call the script with the parameters
& $scriptPath -CSVFilePath ".\RGBAW_Complete.csv" `
    -FixtureGroup 9 `
    -FixturePrefix "RSQ3" `
    -FunctionPathPrefix "Rotosphere Q3/Generated" `
    -NumberOfZones 3 `
    -RedChannelGroupIDs   @(269, 273, 277) `
    -GreenChannelGroupIDs @(270, 274, 278) `
    -BlueChannelGroupIDs  @(271, 275, 279) `
    -WhiteChannelGroupIDs @(272, 276, 280) `
    -StrobeChannelGroupID 264 `
    -FixtureIDs @(9, 10) `
    -RedFixtureChannels   @(0, 5, 10) `
    -GreenFixtureChannels @(1, 6, 11) `
    -BlueFixtureChannels  @(2, 7, 12) `
    -WhiteFixtureChannels @(3, 8, 13) `
    -StrobeFixtureChannel 666 `
    -StrobeValueSlow 15 `
    -StrobeValueMedium 17 `
    -StrobeValueFast 20 `
    -StrobeMaxHz 20

# Strobe channels:  4, 9, 14
# Strobe DMX Range: 0-250
# Strobe Frequency: 20Hz
# Motor Control (Slow to Fast):
# - STOP: 0
# - Left: 1-127
# - STOP: 128
# - RIGHT: 129-255

# Define the file path (generated file) to apply substitutions on
$xmlFilePath = ".\RSQ3.RGBAW_Complete01.xml"

# Read the content of the XML file
$fileContent = Get-Content -Path $xmlFilePath -Raw

# # Replace the single channel strobe placeholder with the 3 channels.
# 666,(\d{1,3})
# 4,$1,9,$1,14,$1
# Replace: 666,(\d{1,3}) => 4,$1,9,$1,14,$1
$fileContent = [regex]::Replace($fileContent, '666,(\d{1,3})', '4,$1,9,$1,14,$1')

# # Sets strobes to 0 for the ChannelGroupsVal
# 269,(\d{1,3}),270,(\d{1,3}),271,(\d{1,3}),272,(\d{1,3}),273,(\d{1,3}),274,(\d{1,3}),275,(\d{1,3}),276,(\d{1,3}),277,(\d{1,3}),278,(\d{1,3}),279,(\d{1,3}),280,(\d{1,3})<
# 269,$1,270,$2,271,$3,272,$4,273,$5,274,$6,275,$7,276,$8,277,$9,278,$10,279,$11,280,$12,264,0<
# Replace: 269,(\d{1,3}),270,(\d{1,3}),... => 269,$1,270,$2,...,264,0<
$fileContent = [regex]::Replace($fileContent, '269,(\d{1,3}),270,(\d{1,3}),271,(\d{1,3}),272,(\d{1,3}),273,(\d{1,3}),274,(\d{1,3}),275,(\d{1,3}),276,(\d{1,3}),277,(\d{1,3}),278,(\d{1,3}),279,(\d{1,3}),280,(\d{1,3})<', '269,$1,270,$2,271,$3,272,$4,273,$5,274,$6,275,$7,276,$8,277,$9,278,$10,279,$11,280,$12,264,0<')

# # Sets the strobes to 0 for the FixtureVal
# ID="(\d{1,3})">0,(\d{1,3}),1,(\d{1,3}),2,(\d{1,3}),3,(\d{1,3}),5,(\d{1,3}),6,(\d{1,3}),7,(\d{1,3}),8,(\d{1,3}),10,(\d{1,3}),11,(\d{1,3}),12,(\d{1,3}),13,(\d{1,3})<
# ID="$1">0,$2,1,$3,2,$4,3,$5,5,$6,6,$7,7,$8,8,$9,10,$10,11,$11,12,$12,13,$13,4,0,9,0,14,0<
# Replace: ID="(\d{1,3})">0,(\d{1,3}),... => ID="$1">0,$2,...,4,0,9,0,14,0<
$fileContent = [regex]::Replace($fileContent, 'ID="(\d{1,3})">0,(\d{1,3}),1,(\d{1,3}),2,(\d{1,3}),3,(\d{1,3}),5,(\d{1,3}),6,(\d{1,3}),7,(\d{1,3}),8,(\d{1,3}),10,(\d{1,3}),11,(\d{1,3}),12,(\d{1,3}),13,(\d{1,3})<', 'ID="$1">0,$2,1,$3,2,$4,3,$5,5,$6,6,$7,7,$8,8,$9,10,$10,11,$11,12,$12,13,$13,4,0,9,0,14,0<')

# # Sets the strobes to the proper values based on the channel offset.
# ,(264|4|9|14),191
# ,$1,188
# Replace: ,(264|4|9|14),191 => ,$1,188
$fileContent = [regex]::Replace($fileContent, ',(264|4|9|14),191', ',$1,188')
# ,(264|4|9|14),216
# ,$1,213
# Replace: ,(264|4|9|14),216 => ,$1,213
$fileContent = [regex]::Replace($fileContent, ',(264|4|9|14),216', ',$1,213')
# ,(264|4|9|14),255
# ,$1,250
# Replace: ,(264|4|9|14),255 => ,$1,250
$fileContent = [regex]::Replace($fileContent, ',(264|4|9|14),255', ',$1,250')

# Save the updated content back to the XML file
Set-Content -Path $xmlFilePath -Value $fileContent

# Notify user of completion
Write-Host "File updated with regex substitutions."

# | Original Value (0-20)  | Mapped Value (0-250)  |
# |------------------------|-----------------------|
# | 0                      | 0                     |
# | 1                      | 13                    |
# | 2                      | 25                    |
# | 3                      | 38                    |
# | 4                      | 50                    |
# | 5                      | 63                    |
# | 6                      | 75                    |
# | 7                      | 88                    |
# | 8                      | 100                   |
# | 9                      | 113                   |
# | 10                     | 125                   |
# | 11                     | 138                   |
# | 12                     | 150                   |
# | 13                     | 163                   |
# | 14                     | 175                   |
# | 15                     | 188                   |
# | 16                     | 200                   |
# | 17                     | 213                   |
# | 18                     | 225                   |
# | 19                     | 238                   |
# | 20                     | 250                   |
# |------------------------|-----------------------|
