# Assuming your script is in the current directory
$scriptPath = ".\Generate-EFXFunctions.ps1"

# Call the script with the parameters
& $scriptPath -CSVFilePath ".\RGBAW_Complete.csv" `
    -FixtureGroup 3 `
    -FixturePrefix "CBH9" `
    -FunctionPathPrefix "ColorBand H9/Generated" `
    -NumberOfZones 3 `
    -RedChannelGroupIDs   @(62, 68, 74) `
    -GreenChannelGroupIDs @(63, 69, 75) `
    -BlueChannelGroupIDs  @(64, 70, 76) `
    -AmberChannelGroupIDs @(65, 71, 77) `
    -WhiteChannelGroupIDs @(66, 72, 78) `
    -UVChannelGroupIDs    @(67, 73, 79) `
    -StrobeChannelGroupID 55 `
    -FixtureIDs @(4, 5) `
    -RedFixtureChannels   @(0, 6, 12) `
    -GreenFixtureChannels @(1, 7, 13) `
    -BlueFixtureChannels  @(2, 8, 14) `
    -AmberFixtureChannels @(3, 9, 15) `
    -WhiteFixtureChannels @(4, 10, 16) `
    -UVFixtureChannels    @(5, 11, 17) `
    -StrobeFixtureChannel 19 `
    -StrobeValueSlow 15 `
    -StrobeValueMedium 17 `
    -StrobeValueFast 20 `
    -StrobeMaxHz 26
