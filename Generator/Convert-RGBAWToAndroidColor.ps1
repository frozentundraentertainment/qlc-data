# https://stackoverflow.com/questions/39949331/how-to-calculate-rgbaw-amber-white-from-rgb-for-leds

$DebugPreference = 'Continue'

# Define the RGB class
class RGB {
    [int]$Red
    [int]$Green
    [int]$Blue

    static [RGB] Default() {
        return [RGB]::new(0, 0, 0)
    }

    RGB([int]$Red, [int]$Green, [int]$Blue) {
        $this.Red = $Red
        $this.Green = $Green
        $this.Blue = $Blue
    }

    # Override ToString method
    [string] ToString() {
        return "Red: {0:D3}, Green: {1:D3}, Blue: {2:D3}" -f $this.Red, $this.Green, $this.Blue
    }
}

# Define the RGBAW class, inheriting from RGB
class RGBAW : RGB {
    [int]$Amber
    [int]$White

    static [RGBAW] Default() {
        return [RGBAW]::new(0, 0, 0, 0, 0)
    }

    RGBAW([int]$Red, [int]$Green, [int]$Blue, [int]$Amber, [int]$White) : base($Red, $Green, $Blue) {
        $this.Amber = $Amber
        $this.White = $White
    }

    # Override ToString method
    [string] ToString() {
        return "{0}, Amber: {1:D3}, White: {2:D3}" -f ([RGB]$this).ToString(), $this.Amber, $this.White
    }
}

function Calculate-Closeness {
    param (
        [RGB]$rgbInput,
        [RGB]$targetRgb,
        [float]$sensitivity = 1.0
    )

    $diff = ([Math]::Abs($rgbInput.Red - $targetRgb.Red) + 
             [Math]::Abs($rgbInput.Green - $targetRgb.Green) + 
             [Math]::Abs($rgbInput.Blue - $targetRgb.Blue))
    
    $normalizedDiff = $diff / (300 * $sensitivity)
    
    $closenessScore = [Math]::Max(0, [Math]::Min(1, 1 - $normalizedDiff))
    
    return $closenessScore
}

function Convert-RGBToRGBAW {
    param (
        [RGB]$rgbColor,
        [float]$amberSensitivity = 1.0,
        [float]$whiteSensitivity = 1.0
    )

    $pureColors = @{
        'red'   = [RGB]::new(255, 0, 0)
        'green' = [RGB]::new(0, 255, 0)
        'blue'  = [RGB]::new(0, 0, 255)
        'amber' = [RGB]::new(255, 128, 0)
        'white' = [RGB]::new(255, 255, 255)
    }

    $scores = @{}
    foreach ($color in $pureColors.Keys) {
        $sensitivity = 1.0
        if ($color -eq 'amber') { $sensitivity = $amberSensitivity }
        elseif ($color -eq 'white') { $sensitivity = $whiteSensitivity }
        
        $scores[$color] = Calculate-Closeness -rgbInput $rgbColor -targetRgb $pureColors[$color] -sensitivity $sensitivity
    }

    $redOut = [Math]::Round($scores['red'] * 100)
    $greenOut = [Math]::Round($scores['green'] * 100)
    $blueOut = [Math]::Round($scores['blue'] * 100)
    $amber = [Math]::Round($scores['amber'] * 100)
    $white = [Math]::Round($scores['white'] * 100)

    return [RGBAW]::new($redOut, $greenOut, $blueOut, $amber, $white)
}

function Convert-RGBAWToRGB {
    param (
        [RGBAW]$RGBAWColor
    )

    Write-Debug $RGBAWColor

    # Calculate the contribution of amber to green
    $amberGreen = [Math]::Round($RGBAWColor.Amber * 0.5)

    # Sum up all contributions
    # Amber applies 100% to RED and 50% to Green.
    # White applies 100% to all RGB.
    $redOut = [Math]::Min(255, $RGBAWColor.Amber + $amberRed + $RGBAWColor.White)
    $greenOut = [Math]::Min(255, $RGBAWColor.Green + $amberGreen + $RGBAWColor.White)
    $blueOut = [Math]::Min(255, $RGBAWColor.Blue + $RGBAWColor.White)
    Write-Debug "AG: $amberGreen, RO: $redOut, GO: $greenOut, BO: $blueOut"

    return [RGB]::new($redOut, $greenOut, $blueOut)
}

# Function to convert RGB to Android Color
function Convert-RGBToAndroidColorHEX {
    <#
    .SYNOPSIS
    Converts RGB values to Android Graphics Color type.

    .DESCRIPTION
    This function takes RGB values, constructs the Android Color in ARGB format with full opacity,
    and returns the color in hexadecimal format.

    .PARAMETER RGBValues
    An instance of the RGB class containing Red, Green, and Blue values.

    .OUTPUTS
    A string representing the Android Color in hexadecimal format (e.g., "0xFFRRGGBB").

    .EXAMPLE
    PS> $androidColor = Convert-RGBToAndroidColorHEX -RGBValues $rgb
    #>

    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNull()]
        [RGB]$RGBValues
    )

    # Construct the Android Color in ARGB format (0xAARRGGBB)
    # Alpha is set to FF (fully opaque)
    $androidColor = 0xFF000000 -bor ($RGBValues.Red -shl 16) -bor ($RGBValues.Green -shl 8) -bor $RGBValues.Blue

    # Return the color in hexadecimal format
    return ("0x{0:X8}" -f $androidColor)
}

function Convert-RGBToAndroidColorINT {
    <#
    .SYNOPSIS
    Converts RGB values to Android Graphics Color type.

    .DESCRIPTION
    This function takes RGB values, constructs the Android Color in ARGB format with full opacity,
    and returns the color as a positive decimal value.

    .PARAMETER RGBValues
    An instance of the RGB class containing Red, Green, and Blue values.

    .OUTPUTS
    A positive decimal value representing the Android Color in ARGB format.

    .EXAMPLE
    PS> $androidColor = Convert-RGBToAndroidColorINT -RGBValues $rgb
    #>

    param(
        [Parameter(Mandatory = $true)]
        [ValidateNotNull()]
        [RGB]$RGBValues
    )

    # Construct the Android Color in ARGB format (0xAARRGGBB)
    # Alpha is set to FF (fully opaque)
    $androidColor = 0xFF000000 -bor ($RGBValues.Red -shl 16) -bor ($RGBValues.Green -shl 8) -bor $RGBValues.Blue

    # If the color is negative, add 0x100000000 to shift it into the positive uint32 range
    if ($androidColor -lt 0) {
        $androidColor = $androidColor + 0x100000000
    }

    # Return the color as a positive unsigned decimal value
    return $androidColor
}

# Function to process the CSV and generate the output
function New-ColorCSV {
    <#
    .SYNOPSIS
    Processes a CSV file containing RGBAW color data and generates an output CSV with RGB and Android Color.

    .DESCRIPTION
    This function reads an input CSV file with columns ID, Name, Red, Green, Blue, Amber, White.
    For each color, it calculates the RGB values and the corresponding Android Color,
    and exports the data to a new CSV file with additional columns: RGB_Red, RGB_Green, RGB_Blue, AndroidColor.

    .PARAMETER InputCSVPath
    The file path to the input CSV.

    .PARAMETER OutputCSVPath
    The file path to the output CSV.

    .EXAMPLE
    PS> New-ColorCSV -InputCSVPath "C:\Colors\InputColors.csv" -OutputCSVPath "C:\Colors\OutputColors.csv"
    #>

    param(
        [Parameter(Mandatory = $true)]
        [string]$InputCSVPath,

        [Parameter(Mandatory = $true)]
        [string]$OutputCSVPath
    )

    # Check if the CSV file exists
    if (-Not (Test-Path -Path $InputCSVPath -PathType Leaf)) {
        Write-Error "Error: CSV file not found at '$InputCSVPath'."
        return
    }

    # Import the CSV file
    try {
        $colors = Import-Csv -Path $InputCSVPath
        Write-Host "CSV file loaded successfully. Found [$($colors.Count)] colors!"
    }
    catch {
        Write-Error "Error importing CSV file: $_"
        return
    }

    # Initialize an array to hold the processed data
    $processedColors = @()

    # Process each color
    foreach ($color in $colors) {
        # Validate and parse input values
        try {
            $id = $color.ID
            $name = $color.Name

            # Convert properties to integers and validate
            $red = [int]$color.Red
            $green = [int]$color.Green
            $blue = [int]$color.Blue
            $amber = [int]$color.Amber
            $white = [int]$color.White

            # Ensure all values are within 0-255
            foreach ($value in @($red, $green, $blue, $amber, $white)) {
                if ($value -lt 0 -or $value -gt 255) {
                    throw "Color values must be between 0 and 255. Found value: $value"
                }
            }
        }
        catch {
            Write-Warning "Skipping color with ID '$($color.ID)' due to invalid data: $_"
            continue
        }

        # Create an RGBAW object
        $rgbaW = [RGBAW]::new($red, $green, $blue, $amber, $white)

        # Convert RGBAW to RGB
        $rgb = Convert-RGBAWToRGB -RGBAWColor $rgbaW
        Write-Debug $rgb

        # Convert RGB to Android Color
        $androidColor = Convert-RGBToAndroidColorINT -RGBValues $rgb

        # Create a custom object with all required properties
        $processedColor = [PSCustomObject]@{
            ID           = $id
            Name         = $name
            Red          = $red
            Green        = $green
            Blue         = $blue
            Amber        = $amber
            White        = $white
            RGB_Red      = $rgb.Red
            RGB_Green    = $rgb.Green
            RGB_Blue     = $rgb.Blue
            AndroidColor = $androidColor
        }

        # Add the processed color to the array
        $processedColors += $processedColor
    }

    # Export the processed data to a new CSV
    try {
        $processedColors | Export-Csv -Path $OutputCSVPath -NoTypeInformation -Encoding UTF8
        Write-Host "Processed data exported successfully to '$OutputCSVPath'."
    }
    catch {
        Write-Error "Error exporting to CSV: $_"
    }
}

# ---------------------------
# Main Script Execution
# ---------------------------

# Define input and output CSV file paths
# Update these paths as needed
$InputCSVPath = ".\RGBAW_Complete.csv"
$OutputCSVPath = ".\RGBAW_RGB_ANDROID_ColorData.csv"

# Call the processing function
New-ColorCSV -InputCSVPath $InputCSVPath -OutputCSVPath $OutputCSVPath
