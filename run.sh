clear
xhost +local:root
docker run -it --rm \
	--name QLCplus \
    --privileged \
    -v /dev/bus/usb:/dev/bus/usb \
	--device /dev/snd \
    --net=host \
	-p 9999:80 \
    -p 6454:6454/UDP \
	--volume='/tmp/.X11-unix:/tmp/.X11-unix:rw' \
	--env=DISPLAY=unix${DISPLAY} \
    -v /home/frozentundra/QLC:/QLC:rw \
    -v ~/.ssh:/root/.ssh:ro \
    -v ~/.ssh:/home/qlc/.ssh:ro \
    -v ~/.gitconfig:/root/.gitconfig:ro \
    --env=QLC_ENABLE=TRUE \
    --env=GITWATCH_ENABLE=TRUE \
    --env=GITWATCH_REMOTE=gitlab \
    --env=HOST_HOSTNAME=$(hostname) \
	repo.arbour.me/fte/qlcplus/master/qlcplus:latest
xhost -local:root
