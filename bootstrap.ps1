#!/usr/bin/env pwsh

###################################
$ConfirmPreference = 'None'

Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted

# Install Powershell Script Analyzer
Install-Module -Name PSScriptAnalyzer -Force